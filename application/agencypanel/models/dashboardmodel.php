<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Dashboardmodel extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    public function userGetAllMember()
    {
        return $this->db->count_all('user');
    }


  
    // New function for filter data 25/02/2019

    public function get_history_filter_data($from_date,$to_date,$agency_id,$calltype)
    {

        $this->db->select('csh.*,nb.payout,nb.currency,nb.number');
        $this->db->from('call_sms_history csh');
        $this->db->join('number nb','nb.number_id = csh.from_numberid');
        if($from_date != "")
        {
            $date = $from_date;
            $res = explode("-", $date);
            $changedDate = $res[2]."-".$res[0]."-".$res[1];

            $this->db->where('DATE(csh.history_date) >=',$changedDate);
        }
        if($to_date != "")
        {
            $date = $to_date;
             $res = explode("-", $date);
            $changedDate = $res[2]."-".$res[0]."-".$res[1];
            $this->db->where('DATE(csh.history_date) <=',$changedDate);
        }
        if($agency_id != "")
        {
            $this->db->where('csh.agency_id',$agency_id);
        }
        if($calltype != "")
        {
            $this->db->where('csh.history_type',$calltype);
        }
        $query = $this->db->get();

        // echo $this->db->last_query(); die;
        return $query->result_array();
    }


 

    // get monthly data
    function get_click($type,$click_template)
    {
    $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        if ($type == "week")
        {
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            $this->db->select('count(url_click_id) as click,Date(createddate) as createddate');
            $this->db->where('Date(createdDate) >=', $startdate);
            $this->db->where('Date(createdDate) <=', $enddate);
            $this->db->where('agency_id', $agencyid);
            if($click_template != 'All')
            {
                // $this->db->where('template_id', $click_template);
                $this->db->where('templatetitle_id', $click_template);
            }
            $this->db->group_by('Date(createddate)');
        }
        elseif ($type == "month")
        {
            $startdate = date('Y-m-01', strtotime('this month'));
            $enddate = date('Y-m-t', strtotime('this month'));
            $this->db->select('count(url_click_id) as click,DAYOFMONTH(createddate) as createddate');
            $this->db->where('Date(createdDate) >=', $startdate);
            $this->db->where('Date(createdDate) <=', $enddate);
            $this->db->where('agency_id', $agencyid);
             if($click_template != 'All')
            {
                // $this->db->where('template_id', $click_template);
                $this->db->where('templatetitle_id', $click_template);
            }
            $this->db->group_by('Date(createddate)');
        }
        elseif ($type == "year")
        {
            $startdate = date('Y-01-01', strtotime('this month'));
            $enddate = date('Y-12-31', strtotime('this month'));
            $this->db->select('count(url_click_id) as click,MONTH(createddate) as createddate');
            $this->db->where('Date(createdDate) >=', $startdate);
            $this->db->where('Date(createdDate) <=', $enddate);
            $this->db->where('agency_id', $agencyid);
             if($click_template != 'All')
            {
                // $this->db->where('template_id', $click_template);
                $this->db->where('templatetitle_id', $click_template);
            }
            $this->db->group_by('MONTH(createddate)');
        }
//        echo $startdate.'/'.$enddate;die;
        //Executing Query
        $query = $this->db->get('url_click');
        //  echo $this->db->last_query();
        return $query->result_array();
    }
    
    
    
    
    function get_call_history_data($type,$call_history)
    {
        
    $sessionarray = $this->session->userdata('agency_admin');
        $agencyid = $sessionarray['agency_id'];
        if ($type == "week")
        {
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            $this->db->select('count(history_id) as historyid,Date(createddate) as createddate');
            $this->db->where('Date(history_date) >=', $startdate);
            $this->db->where('Date(history_date) <=', $enddate);
            $this->db->where('agency_id', $agencyid);    
            $this->db->where('history_type', $call_history);
            $this->db->group_by('Date(createddate)');
        }
        elseif ($type == "month")
        {
            $startdate = date('Y-m-01', strtotime('this month'));
            $enddate = date('Y-m-t', strtotime('this month'));
            $this->db->select('count(history_id) as historyid,DAYOFMONTH(createddate) as createddate');
            $this->db->where('Date(history_date) >=', $startdate);
            $this->db->where('Date(history_date) <=', $enddate);
            $this->db->where('agency_id', $agencyid);
            $this->db->where('history_type', $call_history);
            $this->db->group_by('Date(createddate)');
        }
        elseif ($type == "year")
        {
            $startdate = date('Y-01-01', strtotime('this month'));
            $enddate = date('Y-12-31', strtotime('this month'));
            $this->db->select('count(history_id) as historyid,MONTH(createddate) as createddate');
            $this->db->where('Date(history_date) >=', $startdate);
            $this->db->where('Date(history_date) <=', $enddate);
            $this->db->where('agency_id', $agencyid);
            $this->db->where('history_type', $call_history);
            $this->db->group_by('MONTH(createddate)');
        }
//        echo $startdate.'/'.$enddate;die;
        //Executing Query
        $query = $this->db->get('call_sms_history');
        
        return $query->result_array();
    }
    
    
    
    function get_jeweler($type)
    {

        if ($type == "week")
        {
            $today = date('Y-m-d');
            $week = date("W", strtotime($today));
            $year = date("Y", strtotime($today));
            $weekdates = $this->getWeek($week, $year);
            $startdate = $weekdates['start'];
            $enddate = $weekdates['end'];
            $this->db->select('count(jeweler_id) as jeweler,Date(createddate) as createddate');
            $this->db->where('Date(createdDate) >=', $startdate);
            $this->db->where('Date(createdDate) <=', $enddate);
            $this->db->where('jeweler_active !=', "Delete");
            $this->db->group_by('Date(createddate)');
        }
        elseif ($type == "month")
        {
            $startdate = date('Y-m-01', strtotime('this month'));
            $enddate = date('Y-m-t', strtotime('this month'));
            $this->db->select('count(jeweler_id) as jeweler,DAYOFMONTH(createddate) as createddate');
            $this->db->where('Date(createdDate) >=', $startdate);
            $this->db->where('Date(createdDate) <=', $enddate);
            $this->db->where('jeweler_active !=', "Delete");
            $this->db->group_by('Date(createddate)');
        }
        elseif ($type == "year")
        {
            $startdate = date('Y-01-01', strtotime('this month'));
            $enddate = date('Y-12-31', strtotime('this month'));
            $this->db->select('count(jeweler_id) as jeweler,MONTH(createddate) as createddate');
            $this->db->where('Date(createdDate) >=', $startdate);
            $this->db->where('Date(createdDate) <=', $enddate);
            $this->db->where('jeweler_active !=', "Delete");
            $this->db->group_by('MONTH(createddate)');
        }
//        echo $startdate.'/'.$enddate;die;
        //Executing Query
        $query = $this->db->get('jeweler');
        //echo $this->db->last_query();
        return $query->result_array();
    }

    function getWeek($week, $year)
    {
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 0)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 6)->format('Y-m-d');
        return $result;
    }

    //get module details
    function get_jeweler_details($s_date)
    {
        $this->db->where('Date(createdDate) >=', $s_date);
        $this->db->where('Date(createdDate) <=', $s_date);
        //Executing Query
        $query = $this->db->get('jeweler');
        //echo $this->db->last_query();
        return $query->num_rows();
    }




    
    //Dashboard click data bar chart
    function get_template_click($agencyid,$templateid)
    {
        $this->db->where('agency_id',$agencyid);
        $this->db->where('template_id',$templateid);
       $query = $this->db->get('url_click');
       return $result = $query->result_array();
    }
}

?>
