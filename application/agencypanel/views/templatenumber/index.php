<?php echo $header; ?>
<?php echo $leftmenu; ?>
<style>.templatenumber-status{width: 60px !important;}</style>
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            Manage Template Number
            <!--<?php echo ucfirst($this->uri->segment(1)); ?>-->
            
        </h2>
        <ol class="breadcrumb pull-left">
            <li class="pull-left"><a title= "Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard </a></li>
            <li><a title="Template number"><i class="fa fa-phone"></i> Template number </a></li>
            <?php if ($this->uri->segment(2)) { ?><li><a title="<?php echo ucfirst($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('message')) { ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header">
                        <div class="buttons pull-right">
                            <a title="Add" class="btn btn-info" href="<?php echo site_url('templatenumber/add/') ?>"> <i class="fa fa-plus"></i> Add</a>
                        </div>
                    </div>  
                    <div class="box-body">
                       
                        <?php echo form_open('templatenumber/multipleEvent', array('id' => 'frmtemplatenumber')); ?> 
                        <?php if (!empty($templatenumbers)) { ?>
                            <div class="boxcontent"> 
                                <table id="datatable" class="table table-striped table-bordered dataTable">
                                    <thead>
                                        <tr>
                                            <th>Title</th>
                                            <th>Template</th>
                                            <th>Template Call Number</th>
                                            <th>Template Sms Number</th>
                                            
                                            <!--<th class="text-center">Status</th>-->
                                            <th class="text-center">Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php for ($i = 0; $i < count($templatenumbers); $i++) { 
                                            ?>
                                                <td title="<?php echo stripslashes($templatenumbers[$i]['templates']); ?>"><?php echo stripslashes($templatenumbers[$i]['title']); ?></td>

                                                <td title="<?php echo stripslashes($templatenumbers[$i]['templates']); ?>"><?php echo stripslashes($templatenumbers[$i]['templates']); ?></td>

                                                 <td title="<?php echo stripslashes($templatenumbers[$i]['callnumbers']); ?>"><?php echo stripslashes($templatenumbers[$i]['callnumbers']); ?></td>

                                                <td title="<?php echo stripslashes($templatenumbers[$i]['smsnumbers']); ?>"><?php echo stripslashes($templatenumbers[$i]['smsnumbers']); ?></td>
                                               
                                               <!-- <td class="text-center center" data-th="Status">
                                                    <?php if (($templatenumbers[$i]['templatenumber_active']) == 'Disable') { ?>
                                                        <a class="btn btn-sm templatenumber-status  btn-danger" title="Click to Enable" onclick="viewModal('status', 'Disable',<?php echo stripslashes($templatenumbers[$i]['templatenumber_id']); ?>)" data-toggle="modal" href="#" >Disable</a>
                                                    <?php } ?>
                                                    <?php if (($templatenumbers[$i]['templatenumber_active']) == 'Enable') { ?>
                                                        <a class="btn btn-sm templatenumber-status btn-success  btn-status" title="Click to Disable" onclick="viewModal('status', 'Enable',<?php echo stripslashes($templatenumbers[$i]['templatenumber_id']); ?>)" data-toggle="modal" href="#" >Enable</a>
                                                    <?php } ?>

                                                </td>-->
    <td class="text-center center"> 
<?php 
    $url = base_url().'../template/preview/' . base64_encode($templatenumbers[$i]['template_id']).'/'.base64_encode($templatenumbers[$i]['agency_id'] .'/'. base64_encode($templatenumbers[$i]['act_id']));
    $shorturl = base_url().'../template/index/'.$templatenumbers[$i]['code_new'];
?>
            <span class="btn btn-success preview" title="Preview template" data-val="<?php echo site_url('previewtemplate/view/' . base64_encode($templatenumbers[$i]['template_id']).'/'.base64_encode($templatenumbers[$i]['agency_id']).'/'.base64_encode($templatenumbers[$i]['actid'])); ?>"> <i class="fa fa-eye"></i> </span>

         <a class="btn btn-primary shorturlclick" title="Short url" data-val="<?php echo base64_encode(($templatenumbers[$i]['template_id']))?>,<?php echo base64_encode(($templatenumbers[$i]['agency_id']))?>" onclick="viewModal('getshorturl', 'null','<?php echo ($templatenumbers[$i]['code_new']); ?>','null')"> <i class="fa fa fa-link"></i> </a>
                                                
         <a  class="btn btn-primary" title="Click to get url" onclick="viewModal('geturl', '<?php echo (base64_encode($templatenumbers[$i]['actid'])); ?>','<?php echo (base64_encode($templatenumbers[$i]['template_id'])); ?>','<?php echo (base64_encode($templatenumbers[$i]['agency_id'])); ?>')" data-toggle="modal" href="#" ><i class="fa fa fa-unlink"></i> </a>

         <a class="btn btn-primary" title="Edit" href="<?php echo site_url('templatenumber/edit/' . base64_encode($templatenumbers[$i]['template_id']).'/'.base64_encode($templatenumbers[$i]['actid'])); ?>"> <i class="fa fa fa-pencil-square-o"></i> </a>
                                                    
                                                    <!-- <a  class="btn btn-danger" title="Delete" onclick="viewModal('delete', 'null','<?php echo ($templatenumbers[$i]['template_id']); ?>','<?php echo $agencyid; ?>')" data-toggle="modal" href="#" ><i class="fa fa-trash-o"></i> </a> -->
                <a  class="btn btn-danger" title="Delete" onclick="viewModal('delete', 'null','null','<?php echo ($templatenumbers[$i]['act_id']); ?>')" data-toggle="modal" href="#" ><i class="fa fa-trash-o"></i> </a>
                                                </td>
                                            </tr>
                                            
                                        <?php } ?>
                                    </tbody>
                                </table>
                                <!--                                <button type="button" name="Enable" id="btn1" title="Enable" class="btn btn-sm templatenumber-status btn-success  btn-status" value="Enable"  >Enable</button>
                                                                <button type="button" name="Disable" id="Disable" title="Disable" class="btn btn-sm templatenumber-status  btn-danger" value="Disable" >Disable</button>-->
                               
                                <input type="hidden" name="event" id="event" value=""> 
                                
                                
                            </div> <?php echo form_close(); ?>
                        <?php } else { ?>
                            <!--  start message-yellow -->
                            <div class="alert alert-info">
                                <button type="button" class="close" data-dismiss="alert">&times;</button>
                                No Record Found.

                            </div>
                            <!--  end message-yellow -->
                        <?php } ?>
                    </div>
                </div>
                <!-- end .content --> 
                             
            </div>
            <!-- model for status -->
            <script language="javascript" type="text/javascript">

                //Browser Support Code
                function viewModal(mode, status, id, agencyid) {
                    var ajaxStatus;  // The variable that makes Ajax possible!
                    try {

                        // Opera 8.0+, Firefox, Safari
                        ajaxStatus = new XMLHttpRequest();
                    } catch (e) {

                        // Internet Explorer Browsers
                        try {
                            ajaxStatus = new ActiveXObject("Msxml2.XMLHTTP");
                        } catch (e) {

                            try {
                                ajaxStatus = new ActiveXObject("Microsoft.XMLHTTP");
                            } catch (e) {

                                // Something went wrong
                                alert("Your browser broke!");
                                return false;
                            }
                        }
                    }
                    ajaxStatus.onreadystatechange = function () {

                        if (ajaxStatus.readyState == 4) {
                            var ajaxDisplay = document.getElementById('modalcontent');
//                              alert(ajaxStatus.responseText);
                            ajaxDisplay.innerHTML = ajaxStatus.responseText;
                            $("#statusmodel").modal({show: true});
                        }
                    }
                    ajaxStatus.open("POST", "templatenumber/viewmodal/" + mode + "/" + status + "/" + id + "/" + agencyid, true);
                    ajaxStatus.send(null);
                }

            </script>
            <div id="modalcontent">

            </div>


            <script type="text/javascript">
               $(document).ready(function () {
                    $('#datatable').dataTable({
                        "pagingType": "full_numbers",
                        "bPaginate": true,
                        "bLengthChange": true,
                        "bFilter": true,
                        "bSort": true,
                        "bInfo": true,
                        "bAutoWidth": false,
                        "order": [],
                        "columnDefs": [{
                                "targets": 'no-sort',
                                "orderable": false,
                            }],
                        "columnDefs": [{"targets": 4, "orderable": false}, { "orderable": false}]
                    });
                    if ($('#firstchkbx').hasClass('sorting_asc'))
                    {
                        $('#firstchkbx').removeClass('sorting_asc');
                    }
                    if ($('#firstchkbx').hasClass('sorting_desc'))
                    {
                        $('#firstchkbx').removeClass('sorting_desc');
                    }
                });
            </script>
        </div>
    </section>
                                              
    
    
    
</div>
<?php echo $footer; ?>


<script>
$(document).on('click','.preview',function(){
    var previewurl = $(this).data("val") ;
    if (previewurl == "") {

        e.preventDefault();
    }
    else
    {
        // AJAX code to send data to php file.
        $.ajax({
            type: "POST",
            url: previewurl,
            success: function(data) {
                var winPrint = window.open('', '', 'left=0,top=0,width=1300,height=800,toolbar=0,scrollbars=0,status=0');
                winPrint.document.write(data);
                winPrint.document.close();
              },
            error: function(err) {
                alert('somthing went wrong.');
            }
        });
    }
});
    
</script>



<script>
 
function copyurl()
{
    
   var copyText = document.getElementById("copy");
//   console.log(copyText);
    copyText.select();
    document.execCommand("copy");
    
}

function copyshorturl()
{
     var copyText = document.getElementById("copyshorturl");
//   console.log(copyText);
    copyText.select();
    document.execCommand("copy");
}


    $("#btn1").click(function () {
        var checkboxes = document.querySelectorAll('input[name="check[]"]:checked'), values = [];
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push(el.value);
        });
        if (values == '')
        {
            alert('please select any templatenumber.');
            return false;
        } else
        {
            if (confirm('Are you sure you want to Enable selected items')) {
                $("#event").val('Enable');
                $("#frmtemplatenumber").submit();
                return true;
            }
        }
    });
    $("#Disable").click(function () {
        var checkboxes = document.querySelectorAll('input[name="check[]"]:checked'), values = [];
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push(el.value);
        });
        if (values == '')
        {
            alert('please select any templatenumber.');
            return false;
        } else
        {
            if (confirm('Are you sure you want to Disable selected items')) {
                $("#event").val('Disable');
                $("#frmtemplatenumber").submit();
                return true;
            }
        }
    });
    $("#Delete").click(function () {
        var checkboxes = document.querySelectorAll('input[name="check[]"]:checked'), values = [];
        Array.prototype.forEach.call(checkboxes, function (el) {
            values.push(el.value);
        });
        if (values == '')
        {
            alert('please select any templatenumber.');
            return false;
        } else
        {
            if (confirm('Are you sure you want to Delete selected items')) {
                $("#event").val('Delete');
                $("#frmtemplatenumber").submit();
                return true;
            }
        }
    });
    $('#selecctall').click(function (event) {  //on click
        if (this.checked) { // check select status
            $('.checkbox1').each(function () { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"              
            });
        } else {
            $('.checkbox1').each(function () { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"                      
            });
        }
    });
</script>
