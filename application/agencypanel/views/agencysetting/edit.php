<?php echo $header; ?>
<?php echo $leftmenu; ?>
<base href="<?php echo base_url(); ?>">
<!-- Content Wrapper. Contains user content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?>
            <!--<?php echo ucfirst($this->uri->segment(1)); ?>--> Client Setting
        </h2>
                <ol class="breadcrumb pull-left">
                    <li class="pull-left"><a title="Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a title="Client setting"><i class="fa fa-cog"></i> Client setting </a></li>
       <!-- <?php if ($this->uri->segment(2)) { ?><li><a title="<?php echo ucfirst($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
                </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('message')) { ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                  
                    
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('agencysetting/update/', $attributes);
                    ?>
                    <?php echo form_input(array('name' => 'agency_id', 'type' => 'hidden', 'value' => base64_encode($agencysettings[0]['agency_id']))); ?>
                    
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-9">
                                <div class="row">
                                      <div class="form-group col-md-6" id="firstnameerror">
                                        <label>Client Name</label>&nbsp <span style="color: #a94442;">*</span>
                                            <div class="controls">
                                                <input name="agency_name" data-validation="required length" data-validation-length="6-16" data-validation-error-msg="Please enter Client name." id="agency_name" type="text" class="form-control" value="<?php echo $agencysettings[0]['agency_name']; ?>" />
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-md-6" id="firstnameerror">
                                            <label>Client Email</label>&nbsp <span style="color: #a94442;">*</span>
                                            <div class="controls">
                                                 <input name="agency_email" data-validation="email required" data-validation-error-msg="Please enter client email." id="agency_name" type="text" class="form-control" value="<?php echo $agencysettings[0]['agency_email']; ?>" />
                                            </div>
                                        </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="box-footer" style="padding-left:0px;">
                                        <input type="submit" title="Update" name="update" value="Update" id="update" class="btn btn-primary btn-small" />
                                        <input type="button" title= "Back" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('agencysetting'); ?>'"/>
                                    </div>
                        </div>
                                </div>
                            </div>
                            </div>
                            <script>
                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                    }
                                });
                            </script>
                            <script type="text/javascript">
                                $(function () {
                                    $('input[name="dob"]').daterangepicker({
                                        "singleDatePicker": true,
                                        //                                        timePicker: true,
                                        //                                        timePicker24Hour: true,
                                        //                                        timePickerIncrement: 10,
                                        "drops": "down",
                                        "showDropdowns": true,
                                        locale: {
                                            "format": 'MM-DD-YYYY'
                                        },
                                        "startDate": '01-01-2015',
                                    });

                                });
                            </script>

                        </div><!-- /.box-body -->
                        
                        <?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>

        <?php echo $footer; ?>
<script>

 $("#update").keypress(function(event) {
    if (event.which == 13) {        
        return false;
    }

});
</script>
  
<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

