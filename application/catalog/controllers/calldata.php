<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class calldata extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();

        // include('include.php');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'getcalldata';
        $this->data['section_title'] = 'getcalldata';
        $this->data['site_name'] = 'CALL-DATA';
        $this->data['site_url'] = 'CALL-DATA';

//Load leftsidemenu and save in variable

        $this->load->library('upload');
        $this->load->model('common');
   
    }


    public function index()
    {
        $this->load->view('404');
    }

    // This function is for insert Payout, currency and country in Number table
    public function get_call_data()
    {
        //Fetch Agency APIUsername and APIpassword
        $agencydata = $this->common->get_agency_data_all('agency');
        
        // echo "<pre>"; print_r($this->data['agencydata']); die;
        $totalagency = count($agencydata);
        // print_r($totalagency); die;
        
        for($i=0;$i<$totalagency;$i++)
        {
            $ch = curl_init();
            
            $APIUSERNAME = $agencydata[$i]['api_username'];
            $APIPASSWORD = $agencydata[$i]['api_password'];
            
            $username = $APIUSERNAME;
        	   $password = $APIPASSWORD;
        // 	print_r($username); die;
        	
        // 	$username = "TEST";
        // 	$password = "TEST123";
        	curl_setopt($ch, CURLOPT_URL, "http://api.telcoitlimited.com/BillingGroupsSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec");
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        
        	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        
        	 $result = curl_exec($ch);
    
            $result = json_decode($result);
            
            if(count($result) > 0)
            {
              if(isset($result->status) && $result->status == '401')
              {
                  continue;
              }
              else
              {
                foreach ($result as $row) 
                {
                   $payout = $row->partitionBillingGroupsRates;
                   $currency = $row->partitionCurrency->short_title;
                   $country = $row->country->country;
                    
                   foreach($payout as $rw)
                   {
                       $groupid = $rw->partition_billing_group_id;
                       $ratein = $rw->rate_in;
                       $rateout = $rw->rate_out;
                       
                       $this->common->insert_payout($groupid,$ratein,$currency,$country);   
                   }       
                }
              }
                
            }
            else
            {
                 $this->dt['Message'] = 'No record found';
                 continue;
            }
           
        }
        
        $this->dt['message'] = 'Insert succesfully';
        echo json_encode($this->dt);
      
    }
    
    
    public function call_number_insert()
    {
         //Fetch Agency APIUsername and APIpassword
        $agencydata = $this->common->get_agency_data_all('agency');
        
        // echo "<pre>"; print_r($this->data['agencydata']); die;
        $totalagency = count($agencydata);
        //  print_r($totalagency); die;
        
        for($i=0;$i<$totalagency;$i++)
        {
            $ch = curl_init();
            
            $APIUSERNAME = $agencydata[$i]['api_username'];
            $APIPASSWORD = $agencydata[$i]['api_password'];
            
          	$enddate = date("Y-m-d 23:59:59");
          	$startdate = date('Y-m-d 00:00:00', strtotime('-7 days', strtotime($enddate)));
          	
      		  $username = $APIUSERNAME;
          	$password = $APIPASSWORD;
    
        // 	$username = "TEST";
        // 	$password = "TEST123";
        	curl_setopt($ch, CURLOPT_URL, "http://api.telcoitlimited.com/CdrInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionCdrInboundAllocationSearch[start_epoch]=".$startdate-$enddate."&per-page=100");
        	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        
        	curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
        
        	$result = curl_exec($ch);
        	
        	$result = json_decode($result);
        	
            $history_unique_id = "";
            $tonumber = "";
            $fromnumber = "";
            $history_type = "CALL";
            $history_date = "";
    
            if(count($result) > 0)
            {
              if(isset($result->status) && $result->status == '401')
              {
                  continue;
              }
              else
              {
                foreach ($result as $row) 
                {
                    $history_unique_id = $row->id;
        
                    $tonumber = $row->destination_number;
        
                    $fromnumber = $row->caller_id_number;
        
                    $history_type = 'CALL';
        
                    $history_date = $row->start_stamp;
        
                    $number = $this->get_number_id($fromnumber);
          
// New developement 14-05-2019 start
                    $newcurldata = $this->get_new_curl_data($fromnumber,$history_unique_id,$history_date);
                    // echo "For call data array"; print_r($newcurldata); die;

                    $uniqueurl = $newcurldata['unique_url'];
                    $amount = $newcurldata['amount'];
                    $breadcrumb = $newcurldata['breadcrumb'];
                    $currency = $newcurldata['currency'];
                    $unique_id = $newcurldata['unique_id'];
                    $breadcrumb_created_date = $newcurldata['breadcrumb_created_date'];
                    $urlclick_primary_id = $newcurldata['url_click_primary_id'];

                    $uniqueurl = str_replace("{breadcrumb}",$breadcrumb,$uniqueurl);
                    $uniqueurl = str_replace("{amount}",$amount,$uniqueurl);
                    $uniqueurl = str_replace("{unique_id}",$unique_id,$uniqueurl);
                    $uniqueurl = str_replace("{currency}",$currency,$uniqueurl);
                    
            // New curl for new API

            $ch1 = curl_init();        	
        	curl_setopt($ch1, CURLOPT_URL, $uniqueurl);
        	curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
        	curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");
        	
        	$result = curl_exec($ch1);
            $result = json_decode($result);
            
            // echo "curl called and return response"; echo "<pre>"; print_r($result); die;

            // Update is_used of breadcrumb in url_click table
            $update_data = array(
                'is_used' => 'true'
            );
            $this->common->update_data($update_data, 'url_click', 'url_click_id', $urlclick_primary_id);
            

// New developement 14-05-2019 End

// Check alreadt exist
                    if(count($number) > 0)
                    {   
                        $data = array('calltype'=>'CALL');
                        $this->common->update_data($data, 'number', 'number', $number->number);
                        
                        $numberid = $number->number_id;
                        $agency = $this->get_agency_id($numberid);
                        $agencyid = $agency->agency_id;
        
                        $this->data['existid'] = $this->common->select_database_id('call_sms_history', 'history_unique_id', $history_unique_id, $data = '*');
        
                       // print_r(count($this->data['existid'])); die;
        
                          if(count($this->data['existid']) > 0)
                          {
                            //do nothing
                          }
                          else
                          {
        
                            // print_r('hello'); die;
                              $call_history_data = array(
        
                                  'history_unique_id' => $history_unique_id,
                                  'to_number' => $tonumber,
                                  'from_number' => $fromnumber,
                                  'from_numberid' => $numberid,
                                  'history_date' => $history_date,
                                  'createddate' => date('Y-m-d H:i:s'),
                                  'agency_id' => $agencyid,
                                  'history_type' => 'CALL'
                              );
        
                              //echo "<pre>"; print_r($call_history_data);
                              $this->db->insert('call_sms_history',$call_history_data);
                              $this->dt['Message'] = 'Call data insert successfully';
                              
                          }
                    }
                    else
                    {
                      $this->dt['Message'] = 'Number not matched.';
                       // do nothing
                    }
                        
                  }
              }  
            }
            else
            {
                 $this->dt['Message'] = 'No record found';
            }
        }
        echo json_encode($this->dt);
    }

    public function get_number_id($fromnumber)
    {
        $result = $this->common->get_numberid($fromnumber);
        return $result;
    }

    public function get_agency_id($numberid)
    {
        $result = $this->common->get_agencyid($numberid);
        return $result;
    }
        

    // New function 14-05-2019 4:50 PM

    public function get_new_curl_data($fromnumber,$history_unique_id,$history_date)
    {
        $fromnumber = $this->common->get_numberid($fromnumber);
        if(count($fromnumber) > 0)
        {
            $numberid = $fromnumber->number_id;
            $payout = $fromnumber->payout;
            $currency = $fromnumber->currency;
            $history_unique_id = $history_unique_id;
            $history_date = $history_date;

            $this->data['data'] = $this->common->get_agencyid_templateid_from_templatenumber($numberid);

            if(count($this->data['data']) > 0)
            {
                $templateid = $this->data['data'][0]['template_id'];
                $agencyid = $this->data['data'][0]['agency_id'];
                $templatetitleid = $this->data['data'][0]['act_id'];
    
                // Now get breadcrumb from template title id
                $this->data['breadcrumb'] = $this->common->get_breadcrumb($agencyid,$templatetitleid,$history_date);
                if(count($this->data['breadcrumb']) > 0)
                {
                    $breadcrumb = $this->data['breadcrumb'][0]['breadcrumb'];
                    $createddate = $this->data['breadcrumb'][0]['createddate'];
                    $urlclick_primary_id = $this->data['breadcrumb'][0]['url_click_id'];
        
                    // Now get unique url from template title id
                    $this->data['uniqueurl'] = $this->common->get_unique_url($templatetitleid);
                    $uniqueurl = $this->data['uniqueurl'][0]['unique_url'];
                    
                    $final_array = array(
                        "breadcrumb" => $breadcrumb,
                        "amount" => $payout,
                        "unique_id" => $history_unique_id,
                        "currency" => $currency,
                        "unique_url" => $uniqueurl,
                        "breadcrumb_created_date" => $createddate,
                        "url_click_primary_id" => $urlclick_primary_id
                    );
        
                    return $final_array;
                }
                else{
                    // do nothing
                }
                
            }
            else{
                // Do nothing
            }
        }
        else{
            // do nothing
        }
        
    }

}
    



