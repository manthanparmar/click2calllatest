<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class smsdata extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();

        // include('include.php');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'getsmsdata';
        $this->data['section_title'] = 'getsmsdata';
        $this->data['site_name'] = 'SMS-DATA';
        $this->data['site_url'] = 'SMS-DATA';

//Load leftsidemenu and save in variable

        $this->load->library('upload');
        $this->load->model('common');
   
    }

    public function index()
    {
        $this->load->view('404');
    }

    public function get_sms_data()
    {
        //Fetch Agency APIUsername and APIpassword
        $agencydata = $this->common->get_agency_data_all('agency');
            
         //echo "<pre>"; print_r($agencydata); die;
        $totalagency = count($agencydata);
        
        for($i=0;$i<$totalagency;$i++)
        {
            $ch = curl_init();
            $APIUSERNAME = $agencydata[$i]['api_username'];
            $APIPASSWORD = $agencydata[$i]['api_password'];
            
            $enddate = date("Y-m-d");
            $startdate = date('Y-m-d', strtotime('-7 days', strtotime($enddate)));
      /*      $username = "TESTSMS";
            $password = "TEST123";*/
            $username = $APIUSERNAME;
            $password = $APIPASSWORD;
             
            // print_r($password); die;
            // $url = "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=$startdate+00:00+-+$enddate+23:59";            
            curl_setopt($ch, CURLOPT_URL, "http://api.telcoitlimited.com/SmsInboundAllocationSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&PartitionSmsInboundAllocationSearch[date_range]=$startdate+00:00+-+$enddate+23:59&per-page=100");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
    
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            
            $result = curl_exec($ch);
        
            if (curl_errno($ch)) {
               echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);
            $result = json_decode($result);
             
            $history_unique_id = "";
            $tonumber = "";
            $fromnumber = "";
            $history_type = "SMS";
            $history_date = "";
    

            if(count($result) > 0)
            {
              if(isset($result->status) && $result->status == '401')
              {
                  continue;
              }
              else
              {
                foreach ($result as $row) 
                {
        
                    $history_unique_id = $row->id;
        
                    $tonumber = $row->destination_addr;
        
                    $fromnumber = $row->source_addr;
        
                    $history_type = 'SMS';
        
                    $history_date = $row->start_stamp;
          
                    $number = $this->get_number_id($tonumber);
                 
        // New developement 14-05-2019 start

                    $newcurldata = $this->get_new_curl_data($tonumber,$history_unique_id,$history_date);
            //  echo "For sms data array"; print_r($newcurldata); die;
                    $uniqueurl = $newcurldata['unique_url'];
                    $amount = $newcurldata['amount'];
                    $breadcrumb = $newcurldata['breadcrumb'];
                    $currency = $newcurldata['currency'];
                    $unique_id = $newcurldata['unique_id'];
                    $breadcrumb_created_date = $newcurldata['breadcrumb_created_date'];
                    $urlclick_primary_id = $newcurldata['url_click_primary_id'];

                    
                    $uniqueurl = str_replace("{breadcrumb}",$breadcrumb,$uniqueurl);
                    $uniqueurl = str_replace("{amount}",$amount,$uniqueurl);
                    $uniqueurl = str_replace("{unique_id}",$unique_id,$uniqueurl);
                    $uniqueurl = str_replace("{currency}",$currency,$uniqueurl);

                // New curl for new API

                    $ch1 = curl_init();        	
                    curl_setopt($ch1, CURLOPT_URL, $uniqueurl);
                    curl_setopt($ch1, CURLOPT_RETURNTRANSFER, 1);
                    curl_setopt($ch1, CURLOPT_CUSTOMREQUEST, "GET");
                    
                    $result = curl_exec($ch1);
                    $result = json_decode($result);
                    
                    // echo "curl called and return response"; echo "<pre>"; print_r($result); 

            // Update is_used of breadcrumb in url_click table
                    $update_data = array(
                        'is_used' => 'true'
                    );
                    $this->common->update_data($update_data, 'url_click', 'url_click_id', $urlclick_primary_id);
        // die;

// New developement 14-05-2019 End
                            
        //check number already function
                    if(count($number) > 0)
                    {
                        $data = array('calltype'=>'SMS');
                        $this->common->update_data($data, 'number', 'number', $number->number);
                        
                        $numberid = $number->number_id;
                        $agency = $this->get_agency_id($numberid);

                        if(count($agency) > 0)
                        {
                            $agencyid = $agency->agency_id;
                            $this->data['existid'] = $this->common->select_database_id('call_sms_history', 'history_unique_id', $history_unique_id, $data = '*');
        
                       // print_r(count($this->data['existid'])); die;
        
                          if(count($this->data['existid']) > 0)
                          {
                            //do nothing
                          }
                          else
                          {
                            // print_r('hello'); die;
                              $sms_history_data = array(
                                  'history_unique_id' => $history_unique_id,
                                  'to_number' => $tonumber,
                                  'from_number' => $fromnumber,
                                  'from_numberid' => $numberid,
                                  'history_date' => $history_date,
                                  'createddate' => date('Y-m-d H:i:s'),
                                  'agency_id' => $agencyid,
                                  'history_type' => 'SMS'
                              );
        
                              //echo "<pre>"; print_r($sms_history_data);
                              $this->db->insert('call_sms_history',$sms_history_data);
                              $this->dt['Message'] = 'SMS data insert successfully';
                              
                          }
                        }
                        else
                        {
                          $this->dt['Message'] = 'Agency not matched';
                        }
                       
                    }
                    else
                    {
                        $this->dt['Message'] = 'Number not matched';
                       // do nothing
                    }
                    
                } 
              }
                     
            }
            else
            {
                $this->dt['Message'] = 'No record found';
                continue;
            }
        }
        echo json_encode($this->dt); die;
        
    }

    public function get_number_id($tonumber)
    {
        $result = $this->common->get_numberid($tonumber);
        return $result;
    }

    public function get_agency_id($numberid)
    {
        $result = $this->common->get_agencyid($numberid);
        return $result;
    }



// New function 14-05-2019 4:50 PM
    public function get_new_curl_data($tonumber,$history_unique_id,$history_date)
    {
        $tonumber = $this->common->get_numberid($tonumber);
        if(count($tonumber) > 0)
        {
            $numberid = $tonumber->number_id;
            $payout = $tonumber->payout;
            $currency = $tonumber->currency;
            $history_unique_id = $history_unique_id;
            $history_date = $history_date;


            $this->data['data'] = $this->common->get_agencyid_templateid_from_templatenumber($numberid);
            if(count($this->data['data']) > 0)
            {
                $templateid = $this->data['data'][0]['template_id'];
                $agencyid = $this->data['data'][0]['agency_id'];
                $templatetitleid = $this->data['data'][0]['act_id'];
    
                // Now get breadcrumb from template title id
                $this->data['breadcrumb'] = $this->common->get_breadcrumb($agencyid,$templatetitleid,$history_date);
                if(count($this->data['breadcrumb']) > 0)
                {
                    $breadcrumb = $this->data['breadcrumb'][0]['breadcrumb'];
                    $createddate = $this->data['breadcrumb'][0]['createddate'];
                    $urlclick_primary_id = $this->data['breadcrumb'][0]['url_click_id'];
        
                    // Now get unique url from template title id
                    $this->data['uniqueurl'] = $this->common->get_unique_url($templatetitleid);
                    $uniqueurl = $this->data['uniqueurl'][0]['unique_url'];

                    $final_array = array(
                        "breadcrumb" => $breadcrumb,
                        "amount" => $payout,
                        "unique_id" => $history_unique_id,
                        "currency" => $currency,
                        "unique_url" => $uniqueurl,
                        "breadcrumb_created_date" => $createddate,
                        "url_click_primary_id" => $urlclick_primary_id
                    );
                    return $final_array;
                }
                else{
                    // do nothing
                }
                
            }
            else{
                // do nothing
            }
            
          
        }
        else{
            // do nothing
        }
        
    }

}

