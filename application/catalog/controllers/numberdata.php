<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class numberdata extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();

        // include('include.php');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'getnumberdata';
        $this->data['section_title'] = 'getnumberdata';
        $this->data['site_name'] = 'NUMBER-DATA';
        $this->data['site_url'] = 'NUMBER-DATA';

//Load leftsidemenu and save in variable

        $this->load->library('upload');
        $this->load->model('common');
   
    }


    public function index()
    {
        $this->load->view('404');
    }

    public function get_number_data($j=1)
    {
        // die;
        //Fetch Agency APIUsername and APIpassword
        $agencydata = $this->common->get_agency_data_all('agency');
        
        // echo "<pre>"; print_r($agencydata); die;
        $totalagency = count($agencydata);
        // print_r($totalagency); die;
        
        for($i=0;$i<count($agencydata);$i++)
        {
            $agency_id = $agencydata[$i]['agency_id'];
            $APIUSERNAME = $agencydata[$i]['api_username'];
            $APIPASSWORD = $agencydata[$i]['api_password'];
        
            // print_r($APIUSERNAME); die;
            $ch = curl_init();
            $enddate = date("Y-m-d");
            $startdate = date('Y-m-d', strtotime('-7 days', strtotime($enddate)));
            
           /* $username = "TESTSMS";
            $password = "TEST123";*/
            
            $username = $APIUSERNAME;
            $password = $APIPASSWORD;
            
            // print_r($username); die;
            curl_setopt($ch, CURLOPT_URL, "http://api.telcoitlimited.com/NumbersSearch?authkey=87527533-927b-11e8-872b-509a4c6568ec&per-page=1000&page=$j");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
            
            curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
            
            $result = curl_exec($ch);
            
            if (curl_errno($ch)) {
               echo 'Error:' . curl_error($ch);
            }
            curl_close ($ch);
            $result = json_decode($result);
            
           // echo "<pre>"; print_r($result); die;
            $number = '';
            $partition_billing_group_id = '';
            $addedtime = '';
            $grouptitle = '';
            $sass_service_type_id = '';
            
            if(count($result) > 0)
            {
                if(isset($result->status) && $result->status == '401')
                {
                    continue;
                }
                else
                {
                    foreach ($result as $row) 
                    {
                        $number = $row->number;
                        $partition_billing_group_id = $row->partition_billing_group_id;
                        $addedtime = $row->added_time;
                        $grouptitle = $row->group_title;
                        $sass_service_type_id = $row->sass_service_type_id;

                        $this->data['existid'] = $this->common->select_database_id('number', 'number', $number, $data = '*');
                
                       // print_r(count($this->data['existid'])); die;
                
                        if(count($this->data['existid']) > 0)
                        {
                            //do nothing

                            if($sass_service_type_id == '3' || $sass_service_type_id == 3)
                              {
                                $calltypenew = "CALL";
                              }
                              else if($sass_service_type_id == '4' || $sass_service_type_id == 4)
                              {
                                $calltypenew = "SMS";
                              }
                                
                              $all_number_data = array(
                                'number' => $number,
                                'createdby' => '1',
                                'groupid' => $partition_billing_group_id,
                                'number_active' => 'Enable',
                                'createddate' => $addedtime,
                                'calltype' => $calltypenew,
                                'modifieddate' => date('Y-m-d H:i:s')
                              );
                
                               // echo "<pre>"; print_r($all_number_data);
                             $this->common->update_data($all_number_data, "number", "number", $this->data['existid'][0]['number']);

                            /*  $number_id = $this->data['existid'][0]['number_id'];
                              $agency_number = array(
                                  'agency_id' => $agency_id,
                                  'number_id' => $number_id,
                                  'agencynumber_active' => 'Enable',
                                  'createddate' => date('Y-m-d H:i:s'),
                                  'modifieddate' => date('Y-m-d H:i:s')
                              );

                              $this->common->update_data($agency_number, "agency_number", "number_id", $number_id);*/
                              //$this->db->insert('agency_number',$agency_number);
                                
                        }
                        else
                        {
                          if($sass_service_type_id == '3' || $sass_service_type_id == 3)
                          {
                            $calltypenew = "CALL";
                          }
                          else if($sass_service_type_id == '4' || $sass_service_type_id == 4)
                          {
                            $calltypenew = "SMS";
                          }
                            
                          $all_number_data = array(
                            'number' => $number,
                            'createdby' => '1',
                            'groupid' => $partition_billing_group_id,
                            'number_active' => 'Enable',
                            'createddate' => $addedtime,
                            'calltype' => $calltypenew,
                            'modifieddate' => date('Y-m-d H:i:s')
                          );
            
                           // echo "<pre>"; print_r($all_number_data);
                           $this->db->insert('number',$all_number_data);
                           $number_id = $this->db->insert_id();
                           
                           $agency_number = array(
                              'agency_id' => $agency_id,
                              'number_id' => $number_id,
                              'agencynumber_active' => 'Enable',
                              'createddate' => date('Y-m-d H:i:s'),
                              'modifieddate' => date('Y-m-d H:i:s')
                            );
            
                           
                           $this->db->insert('agency_number',$agency_number);
                           
                        } 
                    } 
                    $this->dt['message'] = 'Insert succesfully';
                }

                if(count($result)>=50)
                {
                    $this->get_number_data($j+1);
                }
            }
            else
            {
                $this->dt['message'] = 'No record found';    
            }
            
            echo json_encode($this->dt);
        }
        
    }
 
}

