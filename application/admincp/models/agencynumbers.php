<?php

Class agencynumbers extends CI_Model {
    
    public function get_agency_number_data() {
        $this->db->where('agencynumber_active !=', 'Delete');
        $this->db->order_by('agencynumber_id', 'DESC');
        $result = $this->db->get('agency_number');
        return $result->result_array();
    }

    public function get_agency_numbers_by_id($agencynumber_id) {
        $this->db->from('agency_number');
        $this->db->where('agency_number.agencynumber_id', $agencynumber_id);
        $this->db->order_by('agencynumber_id', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_agency_number_with_join()
    {
        return $this->db->query("SELECT an.*, GROUP_CONCAT(number.number) as agnnumber, GROUP_CONCAT(an.number_id) as agncynumbernew, ag.agency_name FROM `agency_number` an JOIN agency ag ON an.agency_id = ag.agency_id JOIN number ON number.number_id = an.number_id WHERE agencynumber_active != 'Delete' GROUP BY ag.agency_id  ORDER BY an.agencynumber_id DESC")->result_array();
    }

    public function get_number($numarray)
    {
        
         return $this->db->query("SELECT * FROM `number` WHERE number_id not in ($numarray)")->result_array();
         
        //  echo $this->db->last_query(); die;
    }
    
    public function get_agency($agencyarray)
    {
        return $this->db->query("SELECT * FROM `agency` WHERE agency_id not in ($agencyarray) and agency_active != 'Delete'")->result_array();   
        
    }
    
    public function get_number_id_from_agency_number($agencyid)
    {
        return $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT agency_id SEPARATOR ',') as agid, GROUP_CONCAT(number_id SEPARATOR ',') as numids FROM (`agency_number`) WHERE `agencynumber_active` != 'Delete' AND agency_id != $agencyid ORDER BY `agencynumber_id` DESC")->result_array();
    }

    public function get_agency_number_only()
    {
        return $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT agency_id SEPARATOR ',') as agid, GROUP_CONCAT(number_id SEPARATOR ',') as numberids FROM (`agency_number`) WHERE `agencynumber_active` != 'Delete' ORDER BY `agencynumber_id` DESC")->result_array();
    }
 
 
 
 
 
 
 
 
    public function getnumberids_from_agnum($agencyid)
    {
        return $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT agency_id) as agnids, GROUP_CONCAT(number_id) as nids FROM `agency_number` WHERE agency_id = $agencyid AND agencynumber_active != 'Delete'")->result_array();
    }
    
    public function getnumberids_from_agnum_not_exist($agencyid)
    {
        return $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT agency_id) as agnids, GROUP_CONCAT(number_id) as nids FROM `agency_number` WHERE agency_id != $agencyid AND agencynumber_active != 'Delete'")->result_array();
    }
    
    
}
