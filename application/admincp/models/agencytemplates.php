<?php

Class Agencytemplates extends CI_Model {

    public function get_agencytemplate_data() {
        $this->db->where('agencytemplate_active !=', 'Delete');
        $this->db->order_by('agency_template_id', 'DESC');
        $result = $this->db->get('agency_template');
        return $result->result_array();
    }

    public function get_agencytemplates_by_id($agencytemplate_id) {
        $this->db->from('agency_template');
        $this->db->where('agency_template.agency_template_id', $agencytemplate_id);
        $this->db->order_by('agency_template_id', 'DESC');
        $result = $this->db->get();
        return $result->result_array();
    }
    public function get_agency_template_with_join($id)
    {
        
        if($id != '')
        {
            return $this->db->query("SELECT agency.agency_name,agency_template.*,template.template_name, GROUP_CONCAT(template.template_id) as templateids FROM `agency_template` JOIN agency on agency_template.agency_id = agency.agency_id join template on agency_template.template_id = template.template_id where agency_template.agency_id = $id GROUP BY agency_template.agency_id order by agency_template.agency_template_id DESC")->result_array();   
        }
        else
        {
            return $this->db->query("SELECT agency.agency_name,agency_template.*,template.template_name, GROUP_CONCAT(template.template_name) as templateids FROM `agency_template` JOIN agency on agency_template.agency_id = agency.agency_id join template on agency_template.template_id = template.template_id GROUP BY agency_template.agency_id order by agency_template.agency_template_id DESC")->result_array();
        }
    }

    
    public function get_agency_template_only()
    {
       /* $this->db->select('*,GROUP_CONCAT(DISTINCT agency_id SEPARATOR ",") as agid,GROUP_CONCAT(DISTINCT template_id SEPARATOR ",") as tempids');
        $this->db->from('agency_template');
        $this->db->where('agencytemplate_active !=', 'Delete');
        $this->db->order_by('agency_template_id', 'DESC');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }*/
        return $this->db->query("SELECT *, GROUP_CONCAT(DISTINCT agency_id SEPARATOR ',') as agid, GROUP_CONCAT(DISTINCT template_id SEPARATOR ',') as tempids FROM (`agency_template`) WHERE `agencytemplate_active` != 'Delete' ORDER BY `agency_template_id` DESC")->result_array();
    }
    
    
    public function get_agency_not_in($agencyarray)
    {
		$this->db->select('*');
        $this->db->from('agency');
        $this->db->where('agency_active !=','Delete');
        $this->db->where_not_in('agency_id', $agencyarray);
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return array();
        }
       // $this->db->query("SELECT * FROM `agency` WHERE agency_active != 'Delete' AND agency_id NOT IN ($agencyarray)")->result_array();
        
        echo $this->db->last_query(); die;
    }
   /* 
    public function get_template_not_in($templatearray)
    {
        return $this->db->query("SELECT * FROM `template` WHERE template_id NOT IN ($templatearray)")->result_array();
    }*/

}
