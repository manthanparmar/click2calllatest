<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class agencytemplate extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/agencytemplate_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('agencytemplates');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'Agency template';
        $this->data['section_title'] = 'agencytemplate';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
    }

    public function index()
    {
        $this->data['agencytemplates'] = $this->agencytemplates->get_agency_template_with_join('');
        $this->data['total'] = count($this->data['agencytemplates']);
        $this->load->view('agencytemplate/index', $this->data);
    }

    public function add()
    {
        $this->data['agencytemplate_data'] = $this->common->select_database_id($tablename = 'agency_template', $columnname = 'agencytemplate_active', $columnid = 'Enable', $data = '*');
        // $this->data['uuid'] = strtoupper($this->gen_uuid());
        //$this->data['agency_data'] = $this->common->select_database_id($tablename = 'agency', $columnname = 'agency_active', $columnid = 'Enable', $data = '*');
        
         $this->data['agids'] = $this->agencytemplates->get_agency_template_only();

        $a = array_column($this->data['agids'], 'agid');
        if($a[0] == null)
        {
            $a = array(0);
        }
        
         $a = explode(',',$a[0]);
     /*   $b = array_column($this->data['agids'],'tempids');    
        if($b[0] == null)
        {
            $b = array(0);
        }*/
        
        /*print_r($b); die;
        */
        $this->data['agency_data'] = $this->agencytemplates->get_agency_not_in($a);
        $this->data['template_data'] = $this->common->select_database_id($tablename = 'template', $columnname = 'template_active', $columnid = 'Enable', $data = '*');
        
        $this->load->view('agencytemplate/add', $this->data);
    }

    function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version agencytemplate 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function view($agencytemplateid = NULL)
    {
        if ($agencytemplateid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencytemplate', 'refresh');
        }
        else
        {
            $agencytemplateid = base64_decode($agencytemplateid);
            $agencytemplates = $this->common->select_database_id('agencytemplate', 'agency_template_id', (int) $agencytemplateid, '*');
            $agencytemplates_contact = $this->common->select_database_id('agencytemplate_contact', 'agency_template_id', (int) $agencytemplateid, '*');

            if (count($agencytemplates) > 0)
            {
                $this->data['subscription_data'] = $this->agencytemplates->get_subscription_by_id($agencytemplateid);
                //$this->data['subscription_data'] = array();
                //echo "<pre>";print_r($this->data['subscription_data']);die();
                $this->data['agencytemplates'] = $agencytemplates;
                $this->data['agencytemplates_contact'] = $agencytemplates_contact;
            }
            else
            {
                $this->data['agencytemplates'] = array();
            }

            //Loading View File
            $this->load->view('agencytemplate/view', $this->data);
        }
    }

    public function addagencytemplate()
    {
        $session_data = $this->session->userdata['devclick_admin'];
        $adminid = $session_data['adminid'];

       
            $agencyid = $this->input->post('agency_id');
            $template_ids = $this->input->post('template_ids');
            
            
            if (!empty($template_ids))
            {
                for ($k = 0; $k < count($template_ids); $k++)
                {
                    $agencytemplate_data = array(
                        'agency_id' => $agencyid,
                        'template_id' => $template_ids[$k],
                        'createddate' => date('Y-m-d H:i:s'),
                        'agencytemplate_active' => 'Enable',
                    );
                    $this->common->insert_data($agencytemplate_data, 'agency_template');
                    
                    //Below array is insert shortcode in new table
                   /* $shoreten_code_data = array(
                        'agency_id' => $agencyid,
                        'template_id' => $template_ids[$k],
                        'code' => $this->generateRandomString('6'),
                        'createddate' => date('Y-m-d H:i:s')
                    );
                    
                    $this->common->insert_data($shoreten_code_data, 'shorten_url_code');*/
                    
                }
            
            
                $this->session->set_flashdata('success', 'Client template inserted successfully.');
                redirect('agencytemplate', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agencytemplate', 'refresh');
            }
      
    }

    public function delete($agencyid = NULL)
    {
        if ($agencyid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencytemplate', 'refresh');
        }
        else
        {
            if ($this->common->delete_data('agency_template', 'agency_id', $agencyid))
            {
                $this->session->set_flashdata('success', 'Agency template deleted successfully.');
                redirect('agencytemplate', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('agencytemplate', 'refresh');
            }
        }
    }

    public function edit($agencytemplateid = NULL)
    {
        if ($agencytemplateid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencytemplate', 'refresh');
        }
        else
        {
            $agencytemplateid = base64_decode($agencytemplateid);
            
            
        $this->data['agencytemplate_data'] = $this->common->select_database_id('agency_template', 'agency_template_id', (int) $agencytemplateid, '*');
        $this->data['selected_template'] = $this->agencytemplates->get_agency_template_with_join($this->data['agencytemplate_data'][0]['agency_id']);
        // $this->data['uuid'] = strtoupper($this->gen_uuid()); <?php echo in_array($tmdata['template_id'],$selected_template_array) ?'selected':''; 
        
        $this->data['agency_data'] = $this->common->select_database_id($tablename = 'agency', $columnname = 'agency_active', $columnid = 'Enable', $data = '*');
        $this->data['template_data'] = $this->common->select_database_id($tablename = 'template', $columnname = 'template_active', $columnid = 'Enable', $data = '*');
        
        $this->load->view('agencytemplate/edit', $this->data);
        
          /*  $this->data['agencytemplates'] = $this->common->select_database_id('agency_template', 'agency_template_id', (int) $agencytemplateid, '*');
            $this->load->view('agencytemplate/edit', $this->data);*/
        }
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('agencytemplate_id'))
        {
            $agencytemplateid = base64_decode($this->input->post('agencytemplate_id'));
            
            $agencyid = $this->input->post('agency_id');
            $template_ids = $this->input->post('template_list');
            
            if (!empty($template_ids))
            {
                $this->common->delete_data('agency_template', 'agency_id', $agencyid);
               // $this->common->delete_data('shorten_url_code', 'agency_id', $agencyid);
                for ($k = 0; $k < count($template_ids); $k++)
                {
                    $agencytemplate_data = array(
                        'agency_id' => $agencyid,
                        'template_id' => $template_ids[$k],
                        'createddate' => date('Y-m-d H:i:s'),
                        'agencytemplate_active' => 'Enable',
                    );
                    $this->common->insert_data($agencytemplate_data, 'agency_template');
                    
                    
                    //Below array is insert shortcode in new table
                  /*  $shoreten_code_data = array(
                        'agency_id' => $agencyid,
                        'template_id' => $template_ids[$k],
                        'code' => $this->generateRandomString('6'),
                        'createddate' => date('Y-m-d H:i:s')
                    );
                    
                    $this->common->insert_data($shoreten_code_data, 'shorten_url_code');*/
                    
                }
            }
            $this->session->set_flashdata('success', 'Client template updated successfully.');
            redirect('agencytemplate', 'refresh');
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('agencytemplate', 'refresh');
        }
    }

    public function viewmodal($mode, $type_agencytemplate_active, $id)
    {
        $data['type_status'] = $type_agencytemplate_active;
        $data['mode'] = $mode;
        $data['id'] = $id;

        echo $this->load->view('agencytemplate/modal', $data, true);
    }

    public function changestatus()
    {
        $agencytemplateid = $this->input->post('agencytemplate_id');
        $agencytemplate_active = $this->input->post('agencytemplate_active');
        $status_msg = $this->input->post('status_msg');

        // print_r($this->input->post(NULL,TRUE));die();
        if (isset($this->session->userdata['devclick_admin']))
        {
            if (!$agencytemplateid)
            {
                $this->session->set_flashdata('message', 'Specified id not found.');
                redirect('agencytemplate', 'refresh');
            }
            if ($this->common->update_data(array('agencytemplate_active' => $agencytemplate_active), 'agencytemplate', 'agency_template_id', (int) $agencytemplateid))
            {
                $agencytemplatedata = $this->common->select_database_id('agencytemplate', 'agencytemplate_id', (int) $agencytemplateid, '*');
                /*
                  //$app_name_setting = $this->common->select_database_id('setting','AppSettingID',1);
                  $app_name = $this->common->get_setting_value(1);
                  $app_mail = $this->common->get_setting_value(6);
                  $this->load->library('email');
                  //Loading E-mail config file
                  $this->config->load('email', TRUE);
                  $this->cnfemail = $this->config->item('email');
                  $this->email->initialize($this->cnfemail);
                  $this->email->from($app_mail, $app_name);
                  $this->email->to($agencytemplatedata[0]['agencytemplate_email']);
                  if($agencytemplate_active =="Disable"){
                  $this->email->subject('Rest : agencytemplate Disable');
                  }
                  else
                  {
                  $this->email->subject('rest : agencytemplate Enable');
                  }
                  $mail_body = "<p>Hello&nbsp; " . $agencytemplatedata[0]['agencytemplate_name'] . ",</p>

                  <table>
                  <tbody>
                  <tr>
                  <td> Email</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $agencytemplatedata[0]['agencytemplate_email'] . "</td>
                  </tr>

                  <tr>
                  <td> Message</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $status_msg . ".</td>
                  </tr>

                  </tbody>
                  </table>
                  <p>
                  <span>If you have any question or trouble, please contact an app administrator.</span></p>
                  <p>&nbsp;</p>
                  <p>Regards,<br/>
                  " . $app_name . " Team.</p>";

                  //echo $mail_body;die();
                  $this->email->message($mail_body);

                  $this->email->send(); */

                $this->session->set_flashdata('success', 'Agencytemplate status changed to ' . $agencytemplate_active . '.');
                redirect('agencytemplate', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'There is error in updating agencytemplate status.Try later!');
                redirect('agencytemplate', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong.Please try later!');
            redirect('dashboard', 'refresh');
        }
    }

    public function email_exist($agencytemplateid = NULL)
    {
//        print_r($this->input->post(NULL,TRUE));die();

        $agencytemplatemail = $this->input->post('agencytemplateemail');

        if ($agencytemplatemail != '')
        {

            if ($agencytemplateid == NULL)
            {
                if ($this->common->check_unique_avalibility('agencytemplate', 'agencytemplate_email', $agencytemplatemail, 'agencytemplate_active', 'Delete'))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
            else
            {
                if ($this->common->check_unique_avalibility('agencytemplate', 'agencytemplate_email', $agencytemplatemail, 'agencytemplate_id', (int) base64_decode($agencytemplateid), array('agencytemplate_active !=' => 'Delete')))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
        }
        else
        {
            $response = array('valid' => false, 'message' => 'Enter valid email.');
            echo json_encode($response);
            die();
        }

    }

    public function multipleEvent()
    {
        if ($this->input->post('event') == 'Enable')
        {
            $enableId = $this->input->post('check');
            if ($enableId != '' && $enableId != 0)
            {
                for ($i = 0; $i < count($enableId); $i++)
                {
                    $allId[] = $enableId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'agencytemplate_active', $value = "Enable", 'agency_template_id', 'agencytemplate'))
                {
                    $this->session->set_flashdata('success', 'Agency template status changed to Enable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('agencytemplate', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating agencytemplate status. Try later');
                    redirect('agencytemplate', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Enable. Try later!');
                redirect('agencytemplate', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Disable')
        {
//check permission
// disable multiple item
            $disableId = $this->input->post('check');
            if ($disableId != '' && $disableId != 0)
            {
                for ($i = 0; $i < count($disableId); $i++)
                {
                    $allId[] = $disableId[$i];
                }

                if ($this->common->multipleEvent($allId, $status_columnname = 'agencytemplate_active', $value = "Disable", 'agencytemplate_id', 'agencytemplate'))
                {
                    $this->session->set_flashdata('success', 'Agency template status changed to Disable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('agencytemplate', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating agencytemplate status. Try later');
                    redirect('agencytemplate', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Disable. Try later!');
                redirect('agencytemplate', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Delete')
        {

            $deleteId = $this->input->post('check');
            if ($deleteId != '' && $deleteId != 0)
            {
                for ($i = 0; $i < count($deleteId); $i++)
                {
                    $allId[] = $deleteId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'agencytemplate_active', $value = "Delete", 'agencytemplate_id', 'agencytemplate'))
                {
                    $this->session->set_flashdata('success', 'Agency template deleted successfully.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('agencytemplate', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in deleting agencytemplate. Try later');
                    redirect('agencytemplate', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Delete. Try later!');
                redirect('agencytemplate', 'refresh');
            }
        }
    }
    
    
    
    
    // This function is used to generate shortcode for short url
    
    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    


}

/* End of file agencytemplate.php */
/* Location: ./application/controllers/agencytemplate.php */
