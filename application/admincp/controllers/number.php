<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class number extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     * 		http://example.com/index.php/dashboard
     * 	- or -  
     * 		http://example.com/index.php/dashboard/index
     * 	- or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/number_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('numbers');

//Setting Page Title and Comman Variable
        $this->data['title'] = 'number';
        $this->data['section_title'] = 'number';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
    }

    public function index()
    {
        $this->data['numbers'] = $this->numbers->get_number_data();
        $this->data['total'] = count($this->data['numbers']);
        $this->load->view('number/index', $this->data);
    }

    public function add()
    {
        $this->data['subscription_data'] = $this->common->select_database_id($tablename = 'number', $columnname = 'number_active', $columnid = 'Enable', $data = '*');
        // $this->data['uuid'] = strtoupper($this->gen_uuid());
        $this->load->view('number/add', $this->data);
    }

    function gen_uuid()
    {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    public function view($numberid = NULL)
    {
        if ($numberid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('number', 'refresh');
        }
        else
        {
            $numberid = base64_decode($numberid);
            $numbers = $this->common->select_database_id('number', 'number_id', (int) $numberid, '*');
            $numbers_contact = $this->common->select_database_id('number_contact', 'number_id', (int) $numberid, '*');

            if (count($numbers) > 0)
            {
                $this->data['subscription_data'] = $this->numbers->get_subscription_by_id($numberid);
                //$this->data['subscription_data'] = array();
                //echo "<pre>";print_r($this->data['subscription_data']);die();
                $this->data['numbers'] = $numbers;
                $this->data['numbers_contact'] = $numbers_contact;
            }
            else
            {
                $this->data['numbers'] = array();
            }

            //Loading View File
            $this->load->view('number/view', $this->data);
        }
    }

    public function addnumber()
    {
        $session_data = $this->session->userdata['devclick_admin'];
        $adminid = $session_data['adminid'];

           
            $data = array(
                'number' => $this->input->post('number'),
                'country' => $this->input->post('country'),
                'calltype' => $this->input->post('calltype'),
                'number_active' => 'Enable',
                'createdby' => $adminid,
                'createddate' => date('Y-m-d h:i:s'),
            );

            if ($this->common->insert_data($data, 'number'))
            {
/*
                $app_name = $this->common->get_setting_value(1);
                $app_mail = $this->common->get_setting_value(6);

                // number email
                $mail = $this->common->get_email_byid(1);
                $mailformat = $mail[0]['mailformat'];
                $subject = $mail[0]['subject'];
                //            echo "<pre>";  print_r($mail);die();
                $this->load->library('email');
                //Loading E-mail config file
                $this->config->load('email', TRUE);
                $this->cnfemail = $this->config->item('email');
                $this->email->initialize($this->cnfemail);
                $this->email->from($app_mail, $app_name);
                $this->email->to($email);
                $this->email->subject($subject);
                $mail_body = str_replace("%firstname%", $numbername, str_replace("%email%", $numberemail, str_replace("%password%", $password, str_replace("%appname%", $app_name, ($mailformat)))));
                
              $mail_body = "hello";
                $this->email->message($mail_body);
                $this->email->send();*/

                $this->session->set_flashdata('success', 'Number inserted successfully.');
                redirect('number', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('number', 'refresh');
            }
      
    }

    public function delete($numberid = NULL)
    {
        if ($numberid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('number', 'refresh');
        }
        else
        {
            if ($this->common->update_data(array('number_active' => 'Delete'), 'number', 'number_id', (int) $numberid))
            {
                $this->session->set_flashdata('success', 'Number deleted successfully.');
                redirect('number', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('number', 'refresh');
            }
        }
    }

    public function edit($numberid = NULL)
    {
        if ($numberid == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('number', 'refresh');
        }
        else
        {
            $numberid = base64_decode($numberid);
            $this->data['numbers'] = $this->common->select_database_id('number', 'number_id', (int) $numberid, '*');
            $this->load->view('number/edit', $this->data);
        }
    }

//Updating the record
    public function update()
    {
        if ($this->input->post('number_id'))
        {
            $numberid = base64_decode($this->input->post('number_id'));
           

            $data = array(
                'number' => $this->input->post('number'),
                'country' => $this->input->post('country'),
                'calltype' => $this->input->post('calltype'),
                'number_active' => 'Enable',
                'modifieddate' => date('Y-m-d h:i:s'),
            );

            if ($this->common->update_data($data, 'number', 'number_id', (int) $numberid))
            {
                $this->session->set_flashdata('success', 'Number updated successfully.');
                redirect('number', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'Something went wrong. Please try again.');
                redirect('number', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('number', 'refresh');
        }
    }

    public function viewmodal($mode, $type_number_active, $id)
    {
        $data['type_status'] = $type_number_active;
        $data['mode'] = $mode;
        $data['id'] = $id;

        echo $this->load->view('number/modal', $data, true);
    }

    public function changestatus()
    {
        $numberid = $this->input->post('number_id');
        $number_active = $this->input->post('number_active');
        $status_msg = $this->input->post('status_msg');

        // print_r($this->input->post(NULL,TRUE));die();
        if (isset($this->session->userdata['devclick_admin']))
        {
            if (!$numberid)
            {
                $this->session->set_flashdata('message', 'Specified id not found.');
                redirect('number', 'refresh');
            }
            if ($this->common->update_data(array('number_active' => $number_active), 'number', 'number_id', (int) $numberid))
            {
                $numberdata = $this->common->select_database_id('number', 'number_id', (int) $numberid, '*');
                /*
                  //$app_name_setting = $this->common->select_database_id('setting','AppSettingID',1);
                  $app_name = $this->common->get_setting_value(1);
                  $app_mail = $this->common->get_setting_value(6);
                  $this->load->library('email');
                  //Loading E-mail config file
                  $this->config->load('email', TRUE);
                  $this->cnfemail = $this->config->item('email');
                  $this->email->initialize($this->cnfemail);
                  $this->email->from($app_mail, $app_name);
                  $this->email->to($numberdata[0]['number_email']);
                  if($number_active =="Disable"){
                  $this->email->subject('Rest : number Disable');
                  }
                  else
                  {
                  $this->email->subject('rest : number Enable');
                  }
                  $mail_body = "<p>Hello&nbsp; " . $numberdata[0]['number_name'] . ",</p>

                  <table>
                  <tbody>
                  <tr>
                  <td> Email</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $numberdata[0]['number_email'] . "</td>
                  </tr>

                  <tr>
                  <td> Message</td>
                  <td>&nbsp;:&nbsp;</td>
                  <td>" . $status_msg . ".</td>
                  </tr>

                  </tbody>
                  </table>
                  <p>
                  <span>If you have any question or trouble, please contact an app administrator.</span></p>
                  <p>&nbsp;</p>
                  <p>Regards,<br/>
                  " . $app_name . " Team.</p>";

                  //echo $mail_body;die();
                  $this->email->message($mail_body);

                  $this->email->send(); */

                $this->session->set_flashdata('success', 'Number status changed to ' . $number_active . '.');
                redirect('number', 'refresh');
            }
            else
            {
                $this->session->set_flashdata('message', 'There is error in updating number status.Try later!');
                redirect('number', 'refresh');
            }
        }
        else
        {
            $this->session->set_flashdata('message', 'Something went wrong.Please try later!');
            redirect('dashboard', 'refresh');
        }
    }

    public function email_exist($numberid = NULL)
    {
//        print_r($this->input->post(NULL,TRUE));die();

        $numbermail = $this->input->post('numberemail');

        if ($numbermail != '')
        {

            if ($numberid == NULL)
            {
                if ($this->common->check_unique_avalibility('number', 'number_email', $numbermail, 'number_active', 'Delete'))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
            else
            {
                if ($this->common->check_unique_avalibility('number', 'number_email', $numbermail, 'number_id', (int) base64_decode($numberid), array('number_active !=' => 'Delete')))
                {
                    $response = array('valid' => false, 'message' => 'Email already exist.');
                    echo json_encode($response);
                    die();
                }
                else
                {
                    $response = array('valid' => true);
                    echo json_encode($response);
                    die();
                }
            }
        }
        else
        {
            $response = array('valid' => false, 'message' => 'Enter valid email.');
            echo json_encode($response);
            die();
        }

    }

    public function multipleEvent()
    {
        if ($this->input->post('event') == 'Enable')
        {
            $enableId = $this->input->post('check');
            if ($enableId != '' && $enableId != 0)
            {
                for ($i = 0; $i < count($enableId); $i++)
                {
                    $allId[] = $enableId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'number_active', $value = "Enable", 'number_id', 'number'))
                {
                    $this->session->set_flashdata('success', 'Number status changed to Enable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('number', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating number status. Try later');
                    redirect('number', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Enable. Try later!');
                redirect('number', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Disable')
        {
//check permission
// disable multiple item
            $disableId = $this->input->post('check');
            if ($disableId != '' && $disableId != 0)
            {
                for ($i = 0; $i < count($disableId); $i++)
                {
                    $allId[] = $disableId[$i];
                }

                if ($this->common->multipleEvent($allId, $status_columnname = 'number_active', $value = "Disable", 'number_id', 'number'))
                {
                    $this->session->set_flashdata('success', 'Number status changed to Disable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('number', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in updating number status. Try later');
                    redirect('number', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Disable. Try later!');
                redirect('number', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Delete')
        {

            $deleteId = $this->input->post('check');
            if ($deleteId != '' && $deleteId != 0)
            {
                for ($i = 0; $i < count($deleteId); $i++)
                {
                    $allId[] = $deleteId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'number_active', $value = "Delete", 'number_id', 'number'))
                {
                    $this->session->set_flashdata('success', 'Number deleted successfully.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('number', 'refresh');
                    }
                }
                else
                {
                    $this->session->set_flashdata('error', 'There is error in deleting number. Try later');
                    redirect('number', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Delete. Try later!');
                redirect('number', 'refresh');
            }
        }
    }
    
    
    
    public function checknumber()
    {
        // echo "hi"; die;
        $response = array(
          'valid' => false,
          'message' => 'Post argument "number" is missing.'
        );
        
        if( isset($_POST['number']) ) {
         /* $userRepo = new UserRepository( DataStorage::instance() );
          $number = $userRepo->loadUser( $_POST['number'] );*/
          $number = $_POST['number'];
          
          
          $result = $this->db->query("Select * from number where number = $number AND number_active != 'Delete'");
         
          if( $result->num_rows() > 0 ) {
            
            // Number is exist
            $response = array('valid' => false, 'message' => 'This number is already exist.');
          } else {
            // number is available
            $response = array('valid' => true);
          }
        }
        echo json_encode($response);
    }


}

/* End of file number.php */
/* Location: ./application/controllers/number.php */
