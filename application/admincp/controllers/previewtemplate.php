<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class previewtemplate extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/dashboard
     *  - or -  
     *      http://example.com/index.php/dashboard/index
     *  - or -
     * Since this controller is set as the default controller in 
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/dashboard/<method_name>
     * @see http://codeigniter.com/templatenumber_guide/general/urls.html
     */
    public $data;

    public function __construct()
    {
        parent::__construct();
// Your own constructor code
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');

        $this->load->model('admintemplatenumbers');
        
        $sessionarray = $this->session->userdata('devclick_admin');
        

//Setting Page Title and Comman Variable
        $this->data['title'] = 'previewtemplate';
        $this->data['section_title'] = 'previewtemplate';
        $this->data['site_name'] = '';
        $this->data['site_url'] = '';

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->library('upload');
        $this->load->model('common');
        $this->load->model('previewtemplates');
    }

    public function index()
    {
        $sessionarray = $this->session->userdata('devclick_admin');
        
        
        $this->load->view('previewtemplate/index');
    }

     
    public function shorturl($shortcode = NULL)
    {
        if ($shortcode == NULL)
        {
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('previewtemplate', 'refresh');
        }
        else
        {
            $this->data['template_data'] = $this->previewtemplates->get_template_id_from_shortcode($shortcode);
            
            $templateid = $this->data['template_data'][0]['template_id'];
            $agencyid = $this->data['template_data'][0]['agency_id'];
            $actid = $this->data['template_data'][0]['actid'];
            
            $this->data['templatedata'] = $this->previewtemplates->get_template_preview($templateid);
            // $this->data['numberdata'] = $this->previewtemplates->get_number_from_template_number($agencyid,$templateid);
            $this->data['numberdata'] = $this->previewtemplates->get_number_from_template_number($actid,$templateid);
            // $this->data['smsnumberdata'] = $this->previewtemplates->get_smsnumber_from_template_number($agencyid,$templateid);
            $this->data['smsnumberdata'] = $this->previewtemplates->get_smsnumber_from_template_number($actid,$templateid);

            $templateview = '';
            $templateview = $this->data['templatedata'][0]['template_view'];
         
            $gifpath = $this->config->item('gif_path');
            $url = base_url().'/'.$gifpath;
           
            $i=1;
            
            $countdata = count($this->data['numberdata']);
            
            foreach($this->data['numberdata'] as $numbers)
            {
               $replacenumber = 'tel:'.$numbers['number'];
               $templateview = str_replace("%number$i%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
               $i++;
            }
            $k = 1;
            foreach($this->data['smsnumberdata'] as $smsnumbers)
            {
                $replacenumber = 'sms:'.$smsnumbers['number'];
                $templateview = str_replace("%smsnumber$k%",$replacenumber,$templateview);
                 $templateview = str_replace("%url%",$url,$templateview);
                $k++;
            }
            for($j=$countdata+1;$j<100;$j++)
            {
                $templateview = str_replace("%number$j%",'javascript:void(0)',$templateview);
                $templateview = str_replace("%smsnumber$j%",'javascript:void(0)',$templateview);
            }
            print_r($templateview); 
            die;
            
        }
    }
    
    
    public function view($templateid = NULL,$actid = NULL,$agencyid = NULL)
    {
        if ($templateid == NULL || $agencyid == NULL)
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Specified id not found.');
            redirect('previewtemplate', 'refresh');
        }
        else
        {
            $templateid = base64_decode($templateid);
            $agencyid = base64_decode($agencyid);
            $actid = base64_decode($actid);

            $this->data['templatedata'] = $this->previewtemplates->get_template_preview($templateid);
            
            $this->data['numberdata'] = $this->previewtemplates->get_number_from_template_number($actid,$templateid);

            $this->data['smsnumberdata'] = $this->previewtemplates->get_smsnumber_from_template_number($actid,$templateid);

             $templateview = '';
             $templateview = $this->data['templatedata'][0]['template_mobile_view'];
         
            $gifpath = $this->config->item('gif_path');
            $url = base_url().$gifpath;
            //Click data insert in different table
            
            $urlclick_data = array(
                'agency_id' => $agencyid,
                'template_id' => $templateid,
                'createddate'=> date('Y-m-d H:i:s'),
                'click'=>'YES',
                'templatetitle_id' => $actid,
                'breadcrumb' => $this->generateRandomString('6'),
                'is_used' => 'false'
            );
            
            $this->common->insert_data($urlclick_data, 'url_click'); 
            
            $i=1;
            
            $countdata = count($this->data['numberdata']);
            
            foreach($this->data['numberdata'] as $numbers)
            {
                $replacenumber = 'tel:'.$numbers['number'];
                $templateview = str_replace("%number$i%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
                $i++;
            }
            $k=1;
            foreach($this->data['smsnumberdata'] as $smsnumbers)
            {
                $replacenumber = 'sms:'.$smsnumbers['number'];
                $templateview = str_replace("%smsnumber$k%",$replacenumber,$templateview);
                $templateview = str_replace("%url%",$url,$templateview);
                $k++;
            }
            for($j=$countdata+1;$j<100;$j++)
            {
                $templateview = str_replace("%number$j%",'javascript:void(0)',$templateview);
                $templateview = str_replace("%smsnumber$j%",'javascript:void(0)',$templateview);
            }
            print_r($templateview); 
            die;
            
            //Dont remove below line it is return html view
            //print_r($this->data['templatedata'][0]['template_view']); die;
        }
    }

    function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    

}

/* End of file previewtemplate.php */
/* Location: ./application/controllers/previewtemplate.php */
