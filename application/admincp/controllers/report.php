<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
ob_start();

class Report extends CI_Controller {

    public $paging;
    public $data;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('devclick_admin'))
        {
            //If no session, redirect to login 
            redirect('adminlogin', 'refresh');
        }
        include('include.php');
        $this->load->model('reports');
        $this->data['title'] = 'Report';
        $this->data['section_title'] = 'Report';
        $this->data['site_name'] = $this->settings->get_setting_value(1);
        $this->data['site_url'] = $this->settings->get_setting_value(2);

//Load leftsidemenu and save in variable

        $this->data['topmenu'] = $this->load->view('topmenu', $this->data, true);
        $this->data['leftmenu'] = $this->load->view('leftmenu', $this->data, true);
//Load header and save in variable
        $this->data['header'] = $this->load->view('header', $this->data, true);
        $this->data['footer'] = $this->load->view('footer', $this->data, true);

        $this->load->model('common');
        $this->load->library('upload');
    }

    //load report listing view
    public function index()
    {
        $this->data['report'] = $this->reports->getAlladvertise();
        $this->data['total'] = count($this->data['report']);
        $this->load->view('report/index', $this->data);
    }

    // load add view of report
    public function add()
    {
        $this->load->view('report/add', $this->data);
    }

    //load edit email format view
    public function edit($sub_id = '')
    {
        $log = Logger::getLogger(__CLASS__);
        $sub_id = base64_decode($sub_id);
        if ($sub_id != '' && $sub_id != 0)
        {
            $this->data['report'] = $this->reports->get_report_byid($sub_id);
            if (count($this->data['report']) > 0)
            {
                $this->load->view('report/edit', $this->data);
            }
            else
            {
                $log->error("Try to use invalid id.");
                $this->session->set_flashdata('message', 'Record not found with specified id. Try later!');
                redirect('report', 'refresh');
            }
        }
        else
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Record not found with specified id. Try later!');
            redirect('report', 'refresh');
        }
    }

    //Updating the Record
    public function update()
    {
        $log = Logger::getLogger(__CLASS__);
        //If Old Record Update
        if ($this->input->post('sub_id'))
        {
            $logoimg = '';
            $sub_id = $this->input->post('sub_id');
            if ($sub_id != '' && $sub_id != 0)
            {
                $package_name = trim($this->input->post('package_name'));
                $sub_duration = trim($this->input->post('sub_duration'));
                $sub_cost = trim($this->input->post('sub_cost'));
                $sub_description = trim($this->input->post('sub_description'));

                if (isset($_FILES['sub_image']['name']) && $_FILES['sub_image']['name'] != null)
                {
                    $config['upload_path'] = $this->config->item('report_image_upload_path');
                    $config['allowed_types'] = 'jpeg|png|jpg';
                    $config['file_name'] = str_replace(' ', '_', $package_name) . '_PRO_' . time();
                    $this->upload->initialize($config);

                    if ($this->upload->do_upload('sub_image'))
                    {

                        $upload_data = $this->upload->data();
                        $logoimg = $upload_data['file_name'];
                    }
                    else
                    {
                        $log->warn("Error while uploading image.");
                        $this->session->set_flashdata('message', 'Something went wrong. please try again.');
                        $this->add();
                    }
                }
                else
                {
                    $logoimg = $this->input->post('old_image');
                }

                //Updating Record
                $data = array(
                    'sub_package_name' => $package_name,
                    'sub_duration' => $sub_duration,
                    'sub_cost' => $sub_cost,
                    'sub_description' => $sub_description,
                    'sub_image' => $logoimg,
                    'modifiedby' => '1',
                    'modifieddate' => date('Y-m-d H:i:s'),
                );
                if ($this->common->update_data($data, 'report', 'sub_id', $sub_id))
                {
                    $this->session->set_flashdata('success', 'Report updated successfully.');
                    redirect('report', 'refresh');
                }
                else
                {
                    $log->warn("Error while updating record.");
                    $this->session->set_flashdata('message', 'There is error in updating Report. Try later!');
                    redirect('report', 'refresh');
                }
            }
            else
            {
                $log->error("Try to use invalid id.");
                $this->session->set_flashdata('message', 'Record not found with specified id. Try later!');
                redirect('report', 'refresh');
            }
        }
        else
        {
            $logoimg = '';
            $package_name = trim($this->input->post('package_name'));
            $sub_duration = trim($this->input->post('sub_duration'));
            $sub_cost = trim($this->input->post('sub_cost'));
            $sub_description = trim($this->input->post('sub_description'));

            if (isset($_FILES['sub_image']['name']) && $_FILES['sub_image']['name'] != null)
            {
                $config['upload_path'] = $this->config->item('report_image_upload_path');
                $config['allowed_types'] = 'jpeg|png|jpg';
                $config['file_name'] = str_replace(' ', '_', $package_name) . '_PRO_' . time();
                $this->upload->initialize($config);

                if ($this->upload->do_upload('sub_image'))
                {
                    $upload_data = $this->upload->data();
                    $logoimg = $upload_data['file_name'];
                }
                else
                {
                    $log->warn("Error while uploading image.");
                    $this->session->set_flashdata('message', 'Something went wrong. please try again.');
                    $this->add();
                }
            }
            $data = array(
                'sub_package_name' => $package_name,
                'sub_duration' => $sub_duration,
                'sub_cost' => $sub_cost,
                'sub_description' => $sub_description,
                'sub_image' => $logoimg,
                'sub_active' => 'Disable',
                'createdby' => '1',
                'createddate' => date('Y-m-d H:i:s'),
            );
            if ($this->common->insert_data($data, 'report'))
            {
                $this->session->set_flashdata('success', 'Report inserted successfully.');
                redirect('report', 'refresh');
            }
            else
            {
                $log->warn("Error while inserting record.");
                $this->session->set_flashdata('message', 'Something went wrong. please try again.');
                redirect('report', 'refresh');
            }
        }
    }

    //delete report
    public function delete($sub_id)
    {
        $log = Logger::getLogger(__CLASS__);
        if ($sub_id != '' && $sub_id != 0)
        {
            $data = array(
                'sub_active' => 'Delete',
                'modifiedby' => '1',
                'modifieddate' => date('Y-m-d H:i:s')
            );
            if ($this->common->update_data($data, 'report', 'sub_id', (int) $sub_id))
            {
                $this->session->set_flashdata('success', 'Report deleted successfully.');
                redirect('report', 'refresh');
            }
            else
            {
                $log->warn("Error while updating record.");
                $this->session->set_flashdata('message', 'There is error in deleting Report. Try later!');
                redirect('report', 'refresh');
            }
        }
        else
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'Specific id is not found. Try later!');
            redirect('report', 'refresh');
        }
    }

    // view for report
    public function view($sub_id)
    {
        $log = Logger::getLogger(__CLASS__);
        $sub_id = base64_decode($sub_id);
        if ($sub_id != '' && $sub_id != 0)
        {
            $this->data["report"] = $this->reports->get_report_byid($sub_id);
            if (count($this->data["report"]) > 0)
            {
                $this->load->view('report/view', $this->data);
            }
            else
            {
                $log->error("Try to use invalid id.");
                $this->session->set_flashdata('message', 'There is error in id. Try later!');
                redirect('report', 'refresh');
            }
        }
        else
        {
            $log->error("Try to use invalid id.");
            $this->session->set_flashdata('message', 'There is error in id. Try later!');
            redirect('report', 'refresh');
        }
    }

    public function viewmodal($mode, $type_status, $id)
    {
        $data['type_status'] = $type_status;
        $data['mode'] = $mode;
        $data['id'] = $id;
        echo $this->load->view('report/modal', $data, true);
    }

    public function changestatus()
    {
        $log = Logger::getLogger(__CLASS__);
        $sub_id = $this->input->post('sub_id');
        $sub_active = $this->input->post('sub_active');
        if (isset($this->session->userdata['devclick_admin']))
        {
            if (!$sub_id)
            {
                $log->error("Try to use invalid id.");
                $this->session->set_flashdata('message', 'There is error in id. Try later!');
                redirect('report', 'refresh');
            }
            else
            {
                $data = array(
                    'sub_active' => $sub_active,
                    'modifiedby' => '1',
                    'modifieddate' => date('Y-m-d H:i:s')
                );
                if ($this->common->update_data($data, 'report', 'sub_id', (int) $sub_id))
                {
                    $this->session->set_flashdata('success', 'Report status changed to ' . $sub_active . '.');
                    redirect('report', 'refresh');
                }
                else
                {
                    $log->warn("Error while updating record.");
                    $this->session->set_flashdata('message', 'There is error in updating report status.Try later!');
                    redirect('report', 'refresh');
                }
            }
        }
        else
        {
            $log->error("Session has been expired.");
            $this->session->set_flashdata('message', 'Something went wrong. Please try again.!');
            redirect('report', 'refresh');
        }
    }

    public function multipleEvent()
    {
        $log = Logger::getLogger(__CLASS__);
        if ($this->input->post('event') == 'Enable')
        {
            $enableId = $this->input->post('check');
            if ($enableId != '' && $enableId != 0)
            {
                for ($i = 0; $i < count($enableId); $i++)
                {
                    $allId[] = $enableId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'sub_active', $value = "Enable", 'sub_id', 'report'))
                {
                    $this->session->set_flashdata('success', 'report status changed to Enable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('report', 'refresh');
                    }
                }
                else
                {
                    $log->warn("Error while updating record.");
                    $this->session->set_flashdata('error', 'There is error in updating report status. Try later');
                    redirect('report', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Enable. Try later!');
                redirect('report', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Disable')
        {
//check permission
// disable multiple item
            $disableId = $this->input->post('check');
            if ($disableId != '' && $disableId != 0)
            {
                for ($i = 0; $i < count($disableId); $i++)
                {
                    $allId[] = $disableId[$i];
                }

                if ($this->common->multipleEvent($allId, $status_columnname = 'sub_active', $value = "Disable", 'sub_id', 'report'))
                {
                    $this->session->set_flashdata('success', 'report status changed to Disable.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('report', 'refresh');
                    }
                }
                else
                {
                    $log->warn("Error while updating record.");
                    $this->session->set_flashdata('error', 'There is error in updating report status. Try later');
                    redirect('report', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Disable. Try later!');
                redirect('report', 'refresh');
            }
        }
        if ($this->input->post('event') == 'Delete')
        {

            $deleteId = $this->input->post('check');
            if ($deleteId != '' && $deleteId != 0)
            {
                for ($i = 0; $i < count($deleteId); $i++)
                {
                    $allId[] = $deleteId[$i];
                }
                if ($this->common->multipleEvent($allId, $status_columnname = 'sub_active', $value = "Delete", 'sub_id', 'report'))
                {
                    $this->session->set_flashdata('success', 'report deleted successfully.');
                    if ($_SERVER['HTTP_REFERER'])
                    {
                        redirect($_SERVER['HTTP_REFERER'], 'refresh');
                    }
                    else
                    {
                        redirect('report', 'refresh');
                    }
                }
                else
                {
                    $log->warn("Error while updating record.");
                    $this->session->set_flashdata('error', 'There is error in deleting report. Try later');
                    redirect('report', 'refresh');
                }
            }
            else
            {
                $this->session->set_flashdata('error', 'Select any item to Delete. Try later!');
                redirect('report', 'refresh');
            }
        }
    }

}

?>
