﻿<?php echo $header; ?>
<?php echo $leftmenu; ?>

<base href="<?php echo base_url(); ?>" />
<!-- Content Wrapper. Contains user content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2></h2>
        <ol class="breadcrumb pull-left">
            <li class="pull-left"><a href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a href="<?php echo base_url($this->uri->segment(1)); ?>"><i class="fa fa-university"></i> <?php echo ucfirst($this->uri->segment(1)); ?></a></li>
            <?php if ($this->uri->segment(2)) { ?><li><a href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('message')) { ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <?php if (!empty($jewelers)) { ?>
                        <section class="invoice">
                            <!-- title row -->
                            <div class="row">
                                <div class="col-xs-12">
                                    <input type="button" class="btn btn-warning btn-small pull-right" value="Back" onclick="window.location.href = '<?php echo base_url('jeweler'); ?>'" >
                                    <h1 class="user-header">

                                        <div class="col-md-2">
                                            <img class="pull-left img-responsive" src="<?php
                                            if (file_exists($this->config->item('jeweler_logo_upload_path') . $jewelers[0]['jeweler_logo'])) {
                                                echo (($jewelers[0]['jeweler_logo'] != '') ? $this->config->item('jeweler_logo_upload_path') . $jewelers[0]['jeweler_logo'] : $this->config->item('noimage'));
                                            } else {
                                                echo $this->config->item('noimage');
                                            }
                                            ?>" style="border-radius: 50%; height:120px;" />
                                        </div>

                                        <div> <div class="col-md-10 padng_rmv"> 
                                          <p class="title_heading"> <?php echo ucfirst($jewelers[0]['jeweler_name']); ?>   </p>                                                                                                                                     <!--<small class="pull-right">Date: 2/10/2014</small>-->
                                                <p class="title_address"> <span class="fa fa-home">&nbsp;&nbsp;</span>
                                                 <?php echo stripslashes($jewelers_contact[0]['street']).' '.stripslashes($jewelers_contact[0]['city']).' '.stripslashes($jewelers_contact[0]['country']).' '.stripslashes($jewelers_contact[0]['zipcode']); ?>
                                            </p>
                                            </div>
                                        </div>
                                    </h1>

                                </div>
                                <!-- /.col -->
                            </div>
                            <!-- info row -->
                            <br><br>
                            <div class="row">

                                <div class="col-sm-6">
                                    <span style="font-size:22px;" class="fa fa-user">&nbsp;&nbsp;</span>
                                    <b style="font-size:20px;color:#727272;width:1000px !important">Basic Information :</b> <br><br>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th>Jeweler Name :</th>
                                                <td><?php echo stripslashes($jewelers[0]['jeweler_name']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Jeweler Attention Name :</th>
                                                <td><?php echo stripslashes($jewelers[0]['jeweler_attention_name']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Jeweler UUID :</th>
                                                <td><?php echo stripslashes($jewelers[0]['jeweler_UUID']); ?></td>
                                            </tr>
                                            <tr>
                                                <th>Max no.of Beacon :</th>
                                                <td><?php echo ($jewelers[0]['jeweler_max_beacon']); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-sm-6">
                                    <span style="font-size:22px;" class="fa fa-phone">&nbsp;&nbsp;</span>
                                    <b style="font-size:20px;color:#727272;width:1000px !important">Contact Information :</b> <br><br>
                                    <table class="table">
                                        <tbody>
                                            <tr>
                                                <th class="col-sm-4">Jeweler Email :</th>
                                                <td class="col-sm-8"><?php echo stripslashes($jewelers[0]['jeweler_email']); ?></td>
                                            </tr>
                                            <tr>
                                                <th class="col-sm-4">Jeweler Contact :</th>
                                                <td class="col-sm-8"><?php echo stripslashes($jewelers_contact[0]['contact_no']); ?></td>
                                            </tr>
                                            <tr>
                                                <th class="col-sm-4">Jeweler Address :</th>
                                                <td class="col-sm-8" style="text-align: justify"><?php echo stripslashes($jewelers_contact[0]['street']).' '.stripslashes($jewelers_contact[0]['city']).' '.stripslashes($jewelers_contact[0]['country']).' '.stripslashes($jewelers_contact[0]['zipcode']); ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <br>
    <?php if (!empty($subscription_data)) { ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span style="font-size:22px;" class="fa fa-briefcase">&nbsp;&nbsp;</span>
                                        <b style="font-size:20px;color:#727272;width:1000px !important">Subscription Package Information :</b> <br><br>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>Package Name :</th>
                                                    <td><?php echo stripslashes($subscription_data[0]['sub_package_name']); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Subscription Cost :</th>
                                                    <td><?php echo '$'.($subscription_data[0]['jeweler_sub_cost']); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Subscription Start date :</th>
                                                    <td><?php echo date('M d Y', strtotime($subscription_data[0]['jeweler_sub_start_date'])); ?></td>
                                                </tr>
                                                <tr>
                                                    <th>Subscription Expire Date:</th>
                                                    <td><?php echo date('M d Y', strtotime($subscription_data[0]['jeweler_sub_expiry_date'])); ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>


                                </div>
    <?php } else { ?>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <span style="font-size:22px;" class="fa fa-briefcase">&nbsp;&nbsp;</span>
                                        <b style="font-size:20px;color:#727272;width:1000px !important">Subscription Package Information :</b> <br><br>
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <th>No records</th>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
    <?php } ?>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="col-sm-12">
                                        <input type="button" class="btn btn-warning btn-small" value="Back" onclick="window.location.href = '<?php echo base_url('jeweler'); ?>'">
                                    </div>
                                </div>
                            </div>
                        </section>
                        <br>
                    </div><!-- /.box -->

<?php } else { ?>
                    <div class="box-body">
                        <!--  start message-yellow -->
                        <div class="alert alert-info">  
                            No Record Found.
                        </div>
                    </div>
                    <!--  end message-yellow -->
<?php } ?>
            </div><!--/.col (left) -->
        </div>   <!-- /.row -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->

<?php echo $footer; ?>


