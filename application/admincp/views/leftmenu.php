<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="<?php echo (($this->uri->segment(1) == 'dashboard')?'active':''); ?>">
                <a title="Dashboard" href="<?php echo base_url('dashboard'); ?>">
                  <i class="fa fa-dashboard"></i> <span>Dashboard</span> <!--<small class="label pull-right bg-green">new</small>-->
                </a>
            </li>

            
            <li class="<?php echo (($this->uri->segment(1) == 'setting')?'active':''); ?>">
                <a href="<?php echo base_url('setting'); ?>" title="Settings">
                  <i class="fa fa-cog"></i> <span>Settings</span> <!--<small class="label pull-right bg-green">new</small>-->
                </a>
            </li>
            
           <li class="<?php echo (($this->uri->segment(1) == 'emailformat')?'active':''); ?>">
                <a href="<?php echo base_url('emailformat'); ?>" title="Email Format">
                  <i class="fa fa-envelope"></i> <span>Email Format</span>
                </a>
            </li> 
      
<!--            <li class="<?php echo (($this->uri->segment(1) == 'user')?'active':''); ?>">
                <a href="#" title="User">
                    <i class="fa fa-users"></i> <span>User</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo (($this->uri->segment(1) == 'user') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add user" href="<?php echo base_url('user/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php echo (($this->uri->segment(1) == 'user') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="User" href="<?php echo base_url('user'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>-->
   
           <!-- <li class="<?php// echo (($this->uri->segment(1) == 'number')?'active':''); ?>">
                <a href="#" title="Number">
                    <i class="fa fa-phone"></i> <span>Number</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php //echo (($this->uri->segment(1) == 'number') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add Number" href="<?php echo base_url('number/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php// echo (($this->uri->segment(1) == 'number') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="Number" href="<?php echo base_url('number'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>-->


             <li class="<?php echo (($this->uri->segment(1) == 'agency')?'active':''); ?>">
                <a href="#" title="Client">
                    <i class="fa fa-university"></i> <span>Client</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo (($this->uri->segment(1) == 'agency') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add Client" href="<?php echo base_url('agency/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php echo (($this->uri->segment(1) == 'agency') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="Client" href="<?php echo base_url('agency'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>


            <!--<li class="<?php //echo (($this->uri->segment(1) == 'agencynumber')?'active':''); ?>">-->
            <!--    <a href="#" title="Agency Number">-->
            <!--        <i class="fa fa-mobile"></i> <span>Agency Number</span> <i class="fa fa-angle-left pull-right"></i>-->
            <!--    </a>-->
            <!--    <ul class="treeview-menu">-->
            <!--        <li class="<?php //echo (($this->uri->segment(1) == 'agencynumber') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add Agency number" href="<?php //echo base_url('agencynumber/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>-->
            <!--        <li class="<?php //echo (($this->uri->segment(1) == 'agencynumber') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="Agency number" href="<?php //echo base_url('agencynumber'); ?>"><i class="fa fa-list"></i>List</a></li>-->
            <!--    </ul>-->
            <!--</li>-->
            
            <li class="<?php echo (($this->uri->segment(1) == 'agencytemplate')?'active':''); ?>">
                <a href="#" title="Client Template">
                    <i class="fa fa-bars"></i> <span>Client Template</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo (($this->uri->segment(1) == 'agencytemplate') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add Client template" href="<?php echo base_url('agencytemplate/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php echo (($this->uri->segment(1) == 'agencytemplate') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="Client template" href="<?php echo base_url('agencytemplate'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>
            
            <li class="<?php echo (($this->uri->segment(1) == 'admintemplatenumber')?'active':''); ?>">
                <a href="#" title="Template number">
                    <i class="fa fa-phone"></i> <span>Template number</span> <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo (($this->uri->segment(1) == 'admintemplatenumber') && ($this->uri->segment(2) == 'add')?'active':''); ?>"><a title="Add Template number" href="<?php echo base_url('admintemplatenumber/add'); ?>"><i class="fa fa-plus"></i>Add</a></li>
                    <li class="<?php echo (($this->uri->segment(1) == 'admintemplatenumber') && ($this->uri->segment(2) == '')?'active':''); ?>"><a title="Agency Template number" href="<?php echo base_url('admintemplatenumber'); ?>"><i class="fa fa-list"></i>List</a></li>
                </ul>
            </li>
            
        </ul>
    </section>
</aside>
