<?php echo $header; ?>
<?php echo $leftmenu; ?>

<!-- Content Wrapper. Contains number content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?> <?php echo ucfirst($this->uri->segment(1)); ?>
            <small></small>
        </h2>
        <ol class="breadcrumb pull-left">
            <li class="pull-left"><a title="Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a title = "<?php echo ucfirst($this->uri->segment(1)); ?>" ><i class="fa fa-phone"></i> <?php echo ucfirst($this->uri->segment(1)); ?></a></li>
           <!-- <?php if ($this->uri->segment(2))
            { ?><li><a title="<?php echo ucfirst($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
<?php if ($this->session->flashdata('message'))
{ ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
<?php } ?>
<?php if ($this->session->flashdata('success'))
{ ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
<?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('number/addnumber/', $attributes);
                    ?>

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-10">
                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Number</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="number"  data-validation="length number server"  data-validation-url="<?php echo base_url(); ?>number/checknumber" data-validation-length="10-15" data-validation-error-msg="Please enter valid number." id="number" type="text" class="form-control" value="" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Country</label>&nbsp<span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="country" data-validation-suggestion-nr="1" data-validation-regexp="^([a-zA-Z ]+)$" data-validation="custom" data-validation-error-msg="Please enter country."id="country" type="text" class="form-control suggestions-1 error" value="" />
                                        </div>
                                    </div> 

                                      <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Select Call Type</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <select name="calltype" class="form-control" data-validation="required" data-validation-error-msg="Please select number type.">
                                                <option value="">Select type</option>
                                                <option value="CALL">CALL</option>
                                                <option value="SMS">SMS</option>
                                                <option value="BOTH">BOTH</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>

                            </div>

                         
                            <script>

                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                    $('input[name="country"]').suggestCountry();
                                    },
                                });

                            </script>
                        </div><!-- /.box-body -->
                        <div class="col-md-12">
                            <div class="box-footer" style="padding-left:0px;">
                                <input type="submit" name="add" title="Add" value="Add" id="add" class="btn btn-primary btn-small" />
                                <input type="button" title="Back" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('number'); ?>'"/>
                            </div>
                        </div>
<?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>
    </section>
<?php echo $footer; ?>
    <script>
        $("#add").keypress(function (event) {
            if (event.which == 13) {
                return false;
            }

        });
    </script>

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="<?php echo base_url(); ?>newadmin/dist/js/jquery.geocomplete.min.js" type="text/javascript"></script>


    <script>

        $("#street").geocomplete({
            details: "#details",
            detailsAttribute: "data-geo",
            types: ["geocode", "establishment"]
        });
        function set_value()
        {
            setTimeout(function () {
                var country = $('#address_country1').val();
                var city = $('#address_city1').val();
                var zipcode = $('#address_zipcode1').val();
                $('#country').val(country);
                $('#city').val(city);
                $('#zipcode').val(zipcode);

            }, 1000);

        }
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>