<?php echo $header; ?>
<?php echo $leftmenu; ?>
<base href="<?php echo base_url(); ?>">
<!-- Content Wrapper. Contains user content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?>
            <?php echo ucfirst($this->uri->segment(1)); ?>
        </h2>
                <ol class="breadcrumb pull-left">
                    <li class="pull-left"><a title="Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a title="<?php echo base_url($this->uri->segment(1)); ?>" ><i class="fa fa-phone"></i> <?php echo ucfirst($this->uri->segment(1)); ?></a></li>
      <!--  <?php if ($this->uri->segment(2)) { ?><li><a title="<?php echo base_url($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
                </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('message')) { ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>	<i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('number/update/', $attributes);
                    ?>
                    <?php echo form_input(array('name' => 'number_id', 'type' => 'hidden', 'value' => base64_encode($numbers[0]['number_id']))); ?>


                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-9">
                                <div class="row">
                               
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Number</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="number" data-validation-length="10-15" data-validation="length number" data-validation="custom" data-validation-error-msg="Please enter number." id="number" type="text" class="form-control" value="<?php echo $numbers[0]['number']; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Country</label>&nbsp<span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <input name="country" data-validation-suggestion-nr="1" data-validation="required" data-validation-error-msg="Please enter country."id="country" type="text" class="form-control suggestions-1 error" value="<?php echo $numbers[0]['country']; ?>" />
                                        </div>
                                    </div> 

                                      <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Select Call Type</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls">
                                            <select name="calltype" class="form-control" data-validation="required" data-validation-error-msg="Please select number type.">
                                                <option value="">Select type</option>
                                                <option value="CALL" <?php if($numbers[0]['calltype'] == "CALL"){ echo "selected='selected'";} ?>>CALL</option>
                                                <option value="SMS" <?php if($numbers[0]['calltype'] == "SMS"){ echo "selected='selected'";} ?>>SMS</option>
                                                <option value="BOTH" <?php if($numbers[0]['calltype'] == "BOTH"){ echo "selected='selected'";} ?>>BOTH</option>
                                            </select>

                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="box-footer" style="padding-left:0px">
                                        <input type="submit" title="Update" name="update" value="Update" id="update" class="btn btn-primary btn-small" />
                                        <input type="button" title="Back" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('number'); ?>'"/>
                                    </div>
                        </div>
                                </div>
                            </div>
                            </div>
                            <script>
                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                        $('input[name="country"]').suggestCountry();
                                    }
                                });
                            </script>
                            <script type="text/javascript">
                                $(function () {
                                    $('input[name="dob"]').daterangepicker({
                                        "singleDatePicker": true,
                                        //                                        timePicker: true,
                                        //                                        timePicker24Hour: true,
                                        //                                        timePickerIncrement: 10,
                                        "drops": "down",
                                        "showDropdowns": true,
                                        locale: {
                                            "format": 'MM-DD-YYYY'
                                        },
                                        "startDate": '01-01-2015',
                                    });

                                });
                            </script>

                        </div><!-- /.box-body -->
                        
                        <?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>

        <?php echo $footer; ?>
<script>
 $("#update").keypress(function(event) {
    if (event.which == 13) {        
        return false;
    }

});
</script>

<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

