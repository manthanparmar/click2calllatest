<?php echo $header; ?>
<?php echo $leftmenu; ?>

<!-- Content Wrapper. Contains admintemplatenumber content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?> Template Number
            <!--<?php echo ucfirst($this->uri->segment(1)); ?>-->
            <small></small>
        </h2>
        <ol class="breadcrumb pull-left">
            <li class="pull-left"><a title= "Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
            <li><a title="Template number" ><i class="fa fa-phone"></i> Template number </a></li>
            <!--<?php if ($this->uri->segment(2))
            { ?><li><a title="<?php echo ucfirst($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
<?php if ($this->session->flashdata('message'))
{ ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
    <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
<?php } ?>
<?php if ($this->session->flashdata('success'))
{ ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                    <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
<?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('admintemplatenumber/addadmintemplatenumber/', $attributes);
                    ?>

                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6">

                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                    <label>Template title </label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls" style="width:600px;">
                                            <input name="title" data-validation="required" data-validation-error-msg="Please enter template title." id="title" type="text" placeholder = "Template title" class="form-control" value="" />
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                    <label>Select Agency</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls" style="width:600px;">
                                            <select name="agency_id" class="form-control" onchange="get_template(this.value)" data-validation="required" data-validation-error-msg="Please select template.">
                                                <option value="">Select agency</option>
                                               <?php 
                                                foreach ($agency_data as $agency) { ?>
                                                    <option value="<?php echo $agency['agency_id'];?>"><?php echo $agency['agency_name'];?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                      <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Select Template</label>&nbsp <span style="color: #a94442;">*</span>
                                            <div class="controls" style="width:600px;">
                                                <select name="template_id" id="template_id" class="form-control slct_tmp" data-validation="required" data-validation-error-msg="Please select template.">
                                                    <option value="">Select template</option>
                                                  
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-md-4" id="firstnameerror">
                                            <label>Select Call Numbers</label>&nbsp <span style="color: #a94442;">*</span>
                                            <select id="template_number_list" name = "template_number_list[]" multiple="multiple" style="width: 600px" required="required">
                                                <option value="">-- Select call numbers --</option>
                                                <?php 
                                                    foreach ($number_data as $nmdata) { ?>
                                                    <option value="<?php echo $nmdata['number_id']; ?>"><?php echo $nmdata['number']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                </div>
                                
                                <div class="row">
                                        <div class="form-group col-md-4" id="firstnameerror">
                                            <label>Select SMS Numbers</label>&nbsp <span style="color: #a94442;">*</span>
                                            <select id="template_smsnumber_list" name = "template_smsnumber_list[]" multiple="multiple" style="width: 600px" required="required">
                                                <option value="">-- Select SMS Numbers --</option>
                                                <?php 
                                                    foreach ($smsnumber_data as $nmdata) { ?>
                                                    <option value="<?php echo $nmdata['number_id']; ?>"><?php echo $nmdata['number']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                    <label>URL </label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls" style="width:600px;">
                                            <input name="unique_url" data-validation="required" data-validation-error-msg="Please enter url" id="unique_url" type="text" placeholder = "Unique url" class="form-control" value="" />
                                        </div>
                                    </div>
                                </div>


                                <script>

                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                    },
                                });

                            </script>

                            </div>

                            <div class="col-md-4 mb-view" style="display:none;">
                               <html>
                                <head>
                                <title>Template Preview</title>
                               
                                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
                                <!--<link rel="stylesheet" href="css/temp.css">-->
                                <link rel="stylesheet" href="<?php echo base_url(); ?>newadmin/temp.css" type="text/css"/>
                                
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                
                                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
                                </head>
                                <body>
                                <div class="container phone-view">
                                    <div class="data">
                                        <div class="row">
                                        <center><h1>Template preview</h1></center>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                </body>
                                </html>
                            </div>
                         
                         
                            
                        </div><!-- /.box-body -->
                        <div class="col-md-12">
                            <div class="box-footer" style="padding-left:0px;">
                                <input type="submit" title="Add" name="add" value="Add" id="add" class="btn btn-primary btn-small" />
                                <input type="button" title="Back" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('admintemplatenumber'); ?>'"/>
                            </div>
                        </div>
<?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>
    </section>
<?php echo $footer; ?>

<script>
    function get_template(agencyid)
    {
        
        if (agencyid == "") {
            var $select = $('#template_id');                        
            $select.html('');  
            $select.append('<option value="">Select template</option>');
            e.preventDefault();
        }
        else
        {
           
            // AJAX code to append data to template dropdown.
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admintemplatenumber/get_template_data",
                data:{agencyid:agencyid},
                success: function(data) {
                    data = JSON.parse(data);
                    var $select = $('#template_id');                        
                    $select.html('');  
                    // $select.empty('');
                    $select.append('<option value="">Select template</option>');
                    $.each(data,function(key, value) 
                    {
                        $select.append('<option value=' + value["template_id"] + '>' + value["template_name"] + '</option>');
                    });
                  },
                error: function(err) {
                    alert('somthing went wrong.');
                }
            });  
            
            get_number(agencyid);
            get_sms_number(agencyid);
        }
    }
</script>


<script>
function get_number(agencyid)
{
        if (agencyid == "") {
            var $select = $('#template_number_list');                        
            $select.html('');  
            $select.append('<option value="">Select number</option>');
            e.preventDefault();
        }
        else
        {
      //AJAX code to append number data to number select2
             $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admintemplatenumber/get_number_data",
                data:{agencyid:agencyid},
                success: function(data) {
                    data = JSON.parse(data);
                    var $select = $('#template_number_list');                        
                    $select.html('');  
                    // $select.empty('');
                    $select.append('<option value="">Select number</option>');
                    $.each(data,function(key, value) 
                    {
                        $select.append('<option value=' + value["number_id"] + '>' + value["number"] + '</option>');
                    });
                  },
                error: function(err) {
                    alert('somthing went wrong.');
                }
            });
        }
}
    
</script>


<script>
function get_sms_number(agencyid)
{
        if (agencyid == "") {
            var $select = $('#template_smsnumber_list');                        
            $select.html('');  
            $select.append('<option value="">Select SMS Number</option>');
            e.preventDefault();
        }
        else
        {
      //AJAX code to append number data to number select2
             $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admintemplatenumber/get_smsnumber_data",
                data:{agencyid:agencyid},
                success: function(data) {
                    data = JSON.parse(data);
                    var $select = $('#template_smsnumber_list');                        
                    $select.html('');  
                    // $select.empty('');
                    $select.append('<option value="">Select Sms number</option>');
                    $.each(data,function(key, value) 
                    {
                        $select.append('<option value=' + value["number_id"] + '>' + value["number"] + '</option>');
                    });
                  },
                error: function(err) {
                    alert('somthing went wrong.');
                }
            });
        }
}
    
</script>

 <script>
    $('.slct_tmp').on('change', function() {
      var templateid = this.value;
        if (templateid == "") {
            $('.mb-view').empty();
        }
        else
        {
            // AJAX code to send data to php file.
            $.ajax({
                type: "POST",
                url: "<?php echo base_url();?>admintemplatenumber/mobileview",
                data:{templateid:templateid},
                success: function(data) {
                    $('.mb-view').html(data);
                     $('.mb-view').show();
                  },
                error: function(err) {
                    alert('somthing went wrong.');
                }
            });
        }
    });
</script> 


    <script>
        $("#add").keypress(function (event) {
            if (event.which == 13) {
                return false;
            }

        });
    </script>
    
    <script>
    // $('#template_number_list').sortable({containment:'parent'});
    $('#template_number_list').select2({
        placeholder: 'Select Call Number',
        maximumSelectionLength:4
    });
   
    
    $('#template_smsnumber_list').select2({
        placeholder: 'Select Sms Number',
        maximumSelectionLength:4
    });
    
    </script>

    <script src="http://maps.googleapis.com/maps/api/js?sensor=false&amp;libraries=places"></script>
    <script src="<?php echo base_url(); ?>newadmin/dist/js/jquery.geocomplete.min.js" type="text/javascript"></script>


    <script>
        // $('#agency_numbers_list').multiSelect();

        $("#street").geocomplete({
            details: "#details",
            detailsAttribute: "data-geo",
            types: ["geocode", "establishment"]
        });
        function set_value()
        {
            setTimeout(function () {
                var country = $('#address_country1').val();
                var city = $('#address_city1').val();
                var zipcode = $('#address_zipcode1').val();
                $('#country').val(country);
                $('#city').val(city);
                $('#zipcode').val(zipcode);

            }, 1000);

        }
    </script>
    <script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>