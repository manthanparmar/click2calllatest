<?php echo $header; ?>
<?php echo $leftmenu; ?>
<base href="<?php echo base_url(); ?>">
<!-- Content Wrapper. Contains user content -->
<div class="content-wrapper">
    <section class="content-header">
        <h2>
            <?php echo ucfirst($this->uri->segment(2)); ?> Template Number
            <!--<?php echo ucfirst($this->uri->segment(1)); ?>-->
        </h2>
                <ol class="breadcrumb pull-left">
                    <li class="pull-left"><a title="Dashboard" href="<?php echo base_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                    <li><a title="<?php echo ucfirst($this->uri->segment(1)); ?>" ><i class="fa fa-phone"></i> Template number</a></li>
        <!--<?php if ($this->uri->segment(2)) { ?><li><a title="<?php echo ucfirst($this->uri->segment(2)); ?>" href="<?php echo base_url($this->uri->segment(3)); ?>"> <?php echo ucfirst($this->uri->segment(2)); ?></a></li><?php } ?>-->
                </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <?php if ($this->session->flashdata('message')) { ?>
                    <!--  start message-red -->
                    <div class="box-body">
                        <div class=" alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                            <h4><i class="icon fa fa-ban"></i> Alert!</h4>
                            <?php echo $this->session->flashdata('message'); ?> 
                        </div>
                    </div>
                    <!--  end message-red -->
                <?php } ?>
                <?php if ($this->session->flashdata('success')) { ?>
                    <!--  start message-green -->
                    <div class="alert alert-success alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4>    <i class="icon fa fa-check"></i> Success!</h4>
                        <?php echo $this->session->flashdata('success'); ?>
                    </div>
                    <!--  end message-green -->
                <?php } ?>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <!-- form start -->
                    <?php
                    
                      $numids = $this->data['numberids_array'][0]['numids'];
                      $narray = explode(',',$numids);
                   
                    $agencyarray = array();
                    for($i=0;$i<count($this->data['admintemplatenumbers']);$i++)
                    {
                      $agencyarray[] =  $this->data['admintemplatenumbers'][$i]['agency_id'];  
                    }
                      
                    $templatetitle = $this->data['template_title'][0]['title'];
                    $unique_url = $this->data['template_title'][0]['unique_url'];
                    $templateid = $this->data['admintemplatenumbers'][0]['template_id'];
                    $agencyid = $this->data['admintemplatenumbers'][0]['agency_id'];
                      
         /*           $agnids = $agencynum[0]['agnids'];
                    $agarray = explode(',',$agnids);
                    */
                    
                    $attributes = array('class' => 'has-validation-callback', 'id' => 'frmdesti', 'method' => 'post');
                    echo form_open_multipart('admintemplatenumber/update/', $attributes);
                    ?>
                    <?php echo form_input(array('name' => 'template_id', 'type' => 'hidden', 'value' => base64_encode($admintemplatenumbers[0]['template_id']))); ?>
                     <?php echo form_input(array('name' => 'agency_id', 'type' => 'hidden', 'value' => base64_encode($admintemplatenumbers[0]['agency_id']))); ?>
                    
            
                    <div class="box-body">
                        <div class="form-group">
                            <div class="col-md-6">

                                 <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                    <label>Template title </label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls" style="width:600px;">
                                            <input name="title" data-validation="required" data-validation-error-msg="Please enter valid email." id="title" type="text" class="form-control" value="<?php echo $templatetitle; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <input type="hidden" name="actid" value = "<?php echo $this->data['template_title'][0]['actid']?>">
                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                    <label>Select Agency</label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls" style="width:600px;">
                                            <select name="agency_id" class="form-control" data-validation="required" data-validation-error-msg="Please select template.">
                                                <option value="">Select agency</option>
                                               <?php 
                                                foreach ($agency_data as $agency) { ?>
                                                    <option value="<?php echo $agency['agency_id'];?>" <?php echo $agency['agency_id'] == $agencyid ?'Selected':''; ?> ><?php echo $agency['agency_name'];?></option>
                              
                                                <?php } ?>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="row">
                                      <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Select Template</label>&nbsp <span style="color: #a94442;">*</span>
                                            <div class="controls" style="width:600px;">
                                                <select name="template_id" id="template_id" class="form-control" onchange="getTempl(this.value)" data-validation="required" data-validation-error-msg="Please select template.">
                                                    <option value="">Select template</option>
                                                   <?php
                                                     foreach ($template_data as $template) { ?>
                                                        <option value="<?php echo $template['template_id'];?>" <?php echo $template['template_id'] == $templateid ?'Selected':''; ?> ><?php echo $template['template_name'];?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="form-group col-md-4" id="firstnameerror">
                                            <label>Select Call Numbers</label>&nbsp <span style="color: #a94442;">*</span>
                                            <select id="template_number_list" name = "template_number_list[]" multiple="multiple" style="width: 600px">
                                                <option value="">-- Select Call numbers --</option>
                                                <?php 
                                                    foreach ($number_data as $nmdata) { ?>
                                                    <option value="<?php echo $nmdata['number_id']; ?>" <?php echo in_array($nmdata['number_id'],$narray)?'selected':'';  ?> ><?php echo $nmdata['number']; ?></option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                </div>

                                 <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                        <label>Select SMS Numbers</label>&nbsp <span style="color: #a94442;">*</span>
                                        <select id="template_smsnumber_list" name = "template_smsnumber_list[]" multiple="multiple" style="width: 600px">
                                            <option value="">-- Select sms numbers --</option>
                                            <?php 
                                                foreach ($smsnumber_data as $nmdata) { ?>
                                                <option value="<?php echo $nmdata['number_id']; ?>" <?php echo in_array($nmdata['number_id'],$narray)?'selected':'';  ?> ><?php echo $nmdata['number']; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="form-group col-md-4" id="firstnameerror">
                                    <label>Unique url </label>&nbsp <span style="color: #a94442;">*</span>
                                        <div class="controls" style="width:600px;">
                                            <input name="unique_url" data-validation="required" data-validation-error-msg="Please enter unique url." id="unique_url" type="text" class="form-control" value="<?php echo $unique_url; ?>" />
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                    <div class="box-footer" style="padding-left: 0px;">
                                        <input type="submit" name="update" value="Update" id="update" class="btn btn-primary btn-small" />
                                        <input type="button" class="btn btn-default btn-small" value="Back" onclick="window.location.href = '<?php echo site_url('admintemplatenumber'); ?>'"/>
                                    </div>
                                </div>
                                </div>

                                <script>
                                $.validate({
                                    modules: 'location, date, security, file',
                                    onModulesLoaded: function () {
                                    }
                                });
                            </script>
                            </div>
                            
                            
                            
                            <div class="col-md-4 mb-view" style="display:none;">
                                <html>
                                <head>
                                <title>Template Preview</title>
                               
                                <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
                                <!--<link rel="stylesheet" href="css/temp.css">-->
                                <link rel="stylesheet" href="<?php echo base_url(); ?>newadmin/temp.css" type="text/css"/>
                                
                                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
                                
                                <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
                                </head>
                                <body>
                                <div class="container phone-view">
                                    <div class="data">
                                        <div class="row">
                                        <center><h1>Template preview</h1></center>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                        <div class="row">
                                        <div class="col-md-12">
                                        <a href="/#">
                                        <img style="padding-bottom: 5px;" border="0" alt="W3Schools" src="https://helpx.adobe.com/in/stock/how-to/visual-reverse-image-search/_jcr_content/main-pars/image.img.jpg/visual-reverse-image-search-v2_1000x560.jpg">
                                        </a>
                                        </div>
                                        </div>
                                    </div>
                                </div>
                                </body>
                                </html>
                            </div>
                            
                            
                            
                            </div>
                            
                            <script type="text/javascript">
                                $(function () {
                                    $('input[name="dob"]').daterangepicker({
                                        "singleDatePicker": true,
                                        //                                        timePicker: true,
                                        //                                        timePicker24Hour: true,
                                        //                                        timePickerIncrement: 10,
                                        "drops": "down",
                                        "showDropdowns": true,
                                        locale: {
                                            "format": 'MM-DD-YYYY'
                                        },
                                        "startDate": '01-01-2015',
                                    });

                                });
                            </script>

                        </div><!-- /.box-body -->
                        
                        <?php echo form_close(); ?> 
                    </div><!-- /.box -->

                </div><!--/.col (left) -->
            </div>   <!-- /.row -->

        </div>

        <?php echo $footer; ?>

<script>
 setTimeout(function(){  
     
  var templateid =  $('#template_id').val(); 
    getTempl(templateid);
  }, 150);
   
</script>        
<script>

function getTempl(templateid)
{
    console.log(templateid);
    // var templateid = $('#template_id').val(); 
    if(templateid == '' || templateid == 'undefined')
    {
         $('.mb-view').empty();
        setTimeout(function(){
             timeouttemp();

         }, 150);
    }
    else
    {
          $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>admintemplatenumber/mobileview",
            data:{templateid:templateid},
            success: function(data) {
                $('.mb-view').html(data);
                $('.mb-view').show();
              },
            error: function(err) {
                alert('somthing went wrong.');
            }
        });
    }
     
}


function timeouttemp()
{
    var templateid = $('#template_id').val();
    
    if(templateid == '' || templateid == 'undefined')
    {
         $('.mb-view').empty();
        setTimeout(function(){
             timeouttemp();

         }, 150);
    }
    else
    {
        getTempl(templateid);
    }
}





// old code
$('.slct_edit').on('change', function() {
  var templateid = this.value;
    if (templateid == "") {
        $('.mb-view').empty();
    }
    else
    {
        // AJAX code to send data to php file.
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>admintemplatenumber/mobileview",
            data:{templateid:templateid},
            success: function(data) {
                $('.mb-view').html(data);
              },
            error: function(err) {
                alert('somthing went wrong.');
            }
        });
    }
});
</script>

<script>
 $("#update").keypress(function(event) {
    if (event.which == 13) {        
        return false;
    }

});
</script>
    <script>
    $('#template_number_list').select2({
        placeholder: 'Select Call Number',
        maximumSelectionLength:4
    });
    
      $('#template_smsnumber_list').select2({
        placeholder: 'Select Sms Number',
        maximumSelectionLength:4
    });
    </script>
<script type="text/javascript">
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#view_image').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>

