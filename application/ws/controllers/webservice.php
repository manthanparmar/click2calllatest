<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *');

header('Content-Type:application/json');

class Webservice extends CI_Controller {

    private $data;

    //constructor
    public function __construct()
    {
        parent::__construct();
        $this->load->library('upload');
        $this->load->model('webservice_model');
        $this->load->model('settings');
        $this->load->model('common');
    }

    //defult redirection   
    public function index()
    {
        $this->data['status'] = FALSE;
        $this->data['msg'] = 'Something went wrong. Please try again.';
        echo json_encode($this->data);
        die();
    }

    public function is_loyal()
    {

        $user_id = $this->input->post('user_id');
        $uuid = $this->input->post('jeweler_id');
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        if ($user_id != '' && $jeweler_id != '')
        {
            $sub_data = $this->webservice_model->check_subscription_plan($jeweler_id);
            if (!empty($sub_data))
            {
                if ($sub_data[0]['sub_id'] == 1)
                {
                    $this->data['data']['show_advertise'] = false;
                }
                else
                {
                    $this->data['data']['show_advertise'] = true;
                }
                $user_data = $this->check_is_loyal($user_id);
                if ($user_data != 'NoUser')
                {
                    $this->data['status'] = TRUE;
                    $this->data['msg'] = 'Success';
                    if ($user_data == "Yes")
                    {
                        $this->data['data']['is_loyal'] = true;
                    }
                    else
                    {
                        $this->data['data']['is_loyal'] = false;
                    }
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'User Not found';
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'Something went wrong. Please try again';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'Something went wrong. Please try again';
        }


        echo json_encode($this->data);
        die();
    }

    public function check_is_loyal($user_id)
    {
        $user_data = $this->common->select_database_id('user', 'user_id', $user_id, 'is_loyal');
        if (!empty($user_data))
        {
            if ($user_data[0]['is_loyal'] == "Yes")
            {
                return 'Yes';
            }
            else
            {
                return 'No';
            }
        }
        else
        {
            return 'NoUser';
        }
    }

    //user login function
    public function login()
    {
        $uuid = $this->input->post('jeweler_id');
        $jeweler_id = trim($this->get_jeweler_id_from_uuid($uuid));
        $email = trim($this->input->post('email'));
        $password = base64_encode(trim($this->input->post('password')));
        $device_id = trim($this->input->post('device_id')); // $gcmreg_id
        $device_type = trim($this->input->post('device_type'));
//        echo $email . '/' . $uuid . '/' . $password;
//        die;
        if ($email != '' && $password != '' && $jeweler_id != '')
        {
            $email_result = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
            if (!empty($email_result))
            {
                $id = $email_result[0]['user_id'];
                $response = $this->user_current_status($id);
                if ($response == "Enable")
                {
                    $password_db = $this->common->select_database_id('user', 'user_id', $id, 'user_password');
                    if ($password_db[0]['user_password'] == $password)
                    {
                        $gcmdata = array('user_device_id' => $device_id, 'user_device_type' => $device_type);
                        $this->common->update_data($gcmdata, 'user', 'user_id', $id);
                        $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
//                        echo "<pre>";print_r($login_result);die;
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'You are logged in succesfully';
                        $this->data['data'] = $login_result[0];
                        if ($login_result[0]['user_image'] != '')
                        {
                            $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'];
                        }
                        if ($login_result[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'password is wrong';
                    }
                }
                elseif ($response == "Disable")
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'You are Disabled from admin';
                }
                elseif ($response == "Delete")
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'No user found';
                }
                elseif ($response == "Null")
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'Something went wrong. Please try again.';
                }
                else
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'Something went wrong. Please try again.';
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'email is wrong';
            }
        }
        else if ($email == null && $this->input->post('password') != '' && $jeweler_id != '')
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = ' email is required';
        }
        else if ($email != '' && $this->input->post('password') == '' && $jeweler_id != '')
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = ' password is required';
        }
        else if ($email != '' && $this->input->post('password') != '' && $jeweler_id == '')
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = ' Something went wrong. Please try again';
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = ' email and password are required';
        }

        echo json_encode($this->data);
        die();
    }

    public function login1()
    {
        $email = $this->input->post('email');
        $password = base64_encode($this->input->post('password'));
        $uuid = $this->input->post('jeweler_id');
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);

        $login_with = $this->input->post('login_with');
        $device_id = $this->input->post('device_id'); // $gcmreg_id

        $email_result = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
        if (!empty($email_result))
        {
            $id = $email_result[0]['user_id'];
            $response = $this->user_current_status($id);
            if ($response == "Enable")
            {
                $password_db = $this->common->select_database_id('user', 'user_id', $id, 'user_password');
                if ($password_db[0]['user_password'] == $password)
                {
                    $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
//                        echo "<pre>";print_r($login_result);die;
                    $this->data['status'] = TRUE;
                    $this->data['msg'] = 'You are logged in succesfully';
                    $this->data['data'] = $login_result[0];
                    if ($login_result[0]['user_image'] != '')
                    {
                        $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'];
                    }
                    if ($login_result[0]['is_loyal'] == "Yes")
                    {
                        $this->data['data']['is_loyal'] = true;
                    }
                    else
                    {
                        $this->data['data']['is_loyal'] = false;
                    }
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'password is wrong';
                }
            }
            elseif ($response == "Disable")
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'You are Disabled from admin';
            }
            elseif ($response == "Delete")
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'No user found';
            }
            elseif ($response == "Null")
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'Something went wrong. Please try again.';
            }
            else
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'Something went wrong. Please try again.';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'email is wrong';
        }

        if ($login_with == "Facebook")
        {
            
        }
        elseif ($login_with == "GooglePlus")
        {
            
        }
        elseif ($login_with == "App")
        {
            
        }
        else
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'Something went wrong. Please try again.';
        }

        if ($email != '' && $login_with != '' && $device_id != '')
        {
            $email_result = $this->common->check_unique_avalibility('user', 'user_email', $email, 'user_active !=', 'Delete');
            if (!empty($email_result))
            {
                $id = $email_result[0]['user_id'];
                $response = $this->user_current_status($id);
                if ($response == "Enable")
                {
                    if ($login_with == 'Facebook' || $login_with == "GooglePlus")
                    {
                        $gcmdata = array('user_device_id' => $device_id);
                        $this->common->update_data($gcmdata, 'user', 'user_id', $id);
                        $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'You are logged in succesfully';
                        $this->data['data'] = $login_result[0];
                        if ($login_result[0]['user_image'] != '')
                        {
                            $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'];
                        }
                        if ($login_result[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    else
                    {
                        if ($password != "")
                        {
                            $password_db = $this->common->select_database_id('user', 'user_id', $id, 'user_password');
                            if ($password_db[0]['user_password'] == $password)
                            {
                                $gcmdata = array('user_device_id' => $device_id);
                                $this->common->update_data($gcmdata, 'user', 'user_id', $id);
                                $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
                                $this->data['status'] = TRUE;
                                $this->data['msg'] = 'You are logged in succesfully';
                                $this->data['data'] = $login_result[0];
                                if ($login_result[0]['user_image'] != '')
                                {
                                    $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'];
                                }
                                if ($login_result[0]['is_loyal'] == "Yes")
                                {
                                    $this->data['data']['is_loyal'] = true;
                                }
                                else
                                {
                                    $this->data['data']['is_loyal'] = false;
                                }
                            }
                            else
                            {
                                $this->data['status'] = FALSE;
                                $this->data['msg'] = 'password is wrong';
                            }
                        }
                        else
                        {
                            $this->data['status'] = FALSE;
                            $this->data['msg'] = ' password is required';
                        }
                    }
                }
                elseif ($response == "Disable")
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'You are Disabled from admin';
                }
                elseif ($response == "Delete")
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'No user found';
                }
                elseif ($response == "Null")
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'Something went wrong. Please try again.';
                }
                else
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'Something went wrong. Please try again.';
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'email is wrong';
            }
        }
        else if ($email == null && $this->input->post('password') != '')
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'email is required';
        }
        else if ($email != '' && $this->input->post('password') == '')
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'password is required';
        }
        else if ($email != '' && $this->input->post('password') != '' && ($login_with == '' || $device_id == ''))
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'Something went wrong. Please try again.';
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'email and password are required';
        }

        echo json_encode($this->data);
        die();
    }

    // new user register 
    public function register1()
    {
//        echo "<pre>";
//        print_r($_POST);
//        die;
        $uuid = trim($this->input->post('jeweler_id'));
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        $name = trim($this->input->post('name'));
        $password = trim($this->input->post('password'));
        $email = trim($this->input->post('email'));
        $contact = trim($this->input->post('contact'));
        $device_id = trim($this->input->post('device_id'));
        $login_with = trim($this->input->post('login_with'));
        $device_type = trim($this->input->post('device_type'));
        $profileimg = '';
        $date = date("Y-m-d H:i:s");



        if ($email != '' && $name != '' && $contact != '' && $jeweler_id != '' && $login_with != '')
        {
            // for user image.
            if ((isset($_FILES['user_image']['name'])) && ($_FILES['user_image']['name'] != null))
            {
                $config['upload_path'] = $this->config->item('user_main_upload_path');
                $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                $config['file_name'] = str_replace(' ', '_', $this->input->post('name')) . '_PRO_' . time();
                $this->upload->initialize($config);

                if ($this->upload->do_upload('user_image'))
                {
                    $upload_data = $this->upload->data();
                    //print_r($upload_data);die();
                    $profileimg = $upload_data['file_name'];
                }
                else
                {
                    $this->data['error'] = array('error' => $this->upload->display_errors());
                }
            }
            if ($login_with == "App")
            {
                $pass = base64_encode($password);
            }
            else
            {
                $pass = base64_encode(uniqid());
            }
            $user_data = array(
                'jeweler_id' => $jeweler_id,
                'name' => $name,
                'user_password' => $pass,
                'user_email' => $email,
                'user_contact_number' => $contact,
                'user_image' => $profileimg,
                'user_device_id' => $device_id,
                'user_device_type' => $device_type,
                'user_login_with' => $login_with,
                'user_active' => 'Enable',
                'createdby' => $jeweler_id,
                'createddate' => $date
            );

            $result_mail = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
//            $result_username = $this->common->select_database_id('user','username',$username,'');
            if ((!empty($result_mail)))
            {
                if ($login_with == "App")
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'Email already Exist';
                }
                elseif ($login_with == "Facebook" || $login_with == "GooglePlus")
                {
                    $id = $result_mail[0]['user_id'];
                    $response = $this->user_current_status($id);
                    if ($response == "Enable")
                    {
                        $gcmdata = array('user_device_id' => $device_id, 'user_device_type' => $device_type);
                        $this->common->update_data($gcmdata, 'user', 'user_id', $id);
                        $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
//                        echo "<pre>";print_r($login_result);die;
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'You are logged in succesfully';
                        $this->data['data'] = $login_result[0];
                        if ($login_result[0]['user_image'] != '')
                        {
                            $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'];
                        }
                        if ($login_result[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    elseif ($response == "Disable")
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'You are Disabled.Please contact to admin.';
                    }
                    elseif ($response == "Delete")
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'No user found';
                    }
                    elseif ($response == "Null")
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'Something went wrong. Please try again.';
                    }
                    else
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'Something went wrong. Please try again.';
                    }
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'Something went wrong. Please try again';
                }
            }
            else
            {
                $result = $this->common->insert_data($user_data, 'user');
                if ($result == 1)
                {
                    $user_id = $this->db->insert_id();
                    $user_data = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id,name,user_image,is_loyal');
//                    $app_name = $this->common->get_setting_value(1);
//                    $app_mail = $this->common->get_setting_value(6);
//
//                    $mail = $this->common->get_email_byid(2);
//                    $mailformat = $mail[0]['mailformat'];
//                    $subject = $mail[0]['subject'];
//                    // admin mail
//                    $this->load->library('email');
//                    //Loading E-mail config file
//                    $this->config->load('email', TRUE);
//                    $this->cnfemail = $this->config->item('email');
//                    $this->email->initialize($this->cnfemail);
//                    $this->email->from($app_mail, $app_name);
//                    $this->email->to($app_mail);
//                    $this->email->subject($subject);
//                    $mail_body = str_replace("%firstname%", $name, str_replace("%email%", $email, str_replace("%appname%", $app_name, ($mailformat))));
//                    $this->email->message($mail_body);
//                    $this->email->send();

                    $this->data['status'] = TRUE;
                    $this->data['msg'] = 'User registered successfully';
                    $this->data['data'] = $user_data[0];
                    if ($user_data[0]['user_image'] != '')
                    {
                        $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $user_data[0]['user_image'];
                    }
                    if ($user_data[0]['is_loyal'] == "Yes")
                    {
                        $this->data['data']['is_loyal'] = true;
                    }
                    else
                    {
                        $this->data['data']['is_loyal'] = false;
                    }
                }
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'All fields required';
        }
        echo json_encode($this->data);
        die();
    }

    public function registers()
    {
//        echo "<pre>";
//        print_r($_POST);
//        die;
        $uuid = trim($this->input->post('jeweler_id'));
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        $name = trim($this->input->post('name'));
        $password = trim($this->input->post('password'));
        $email = trim($this->input->post('email'));
        $contact = trim($this->input->post('contact'));
        $device_id = trim($this->input->post('device_id'));
        $login_with = trim($this->input->post('login_with'));
        $device_type = trim($this->input->post('device_type'));
        $social_id = trim($this->input->post('social_id'));
        $profileimg = '';
        $date = date("Y-m-d H:i:s");

        if ($login_with == "App")
        {
            if ($email != '' && $name != '' && $password != '' && $contact != '' && $jeweler_id != '' && $login_with != '' && $device_type != '')
            {
                $result_mail = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
                if ((!empty($result_mail)))
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'Emailid already exist.';
                }
                else
                {
                    if ((isset($_FILES['user_image']['name'])) && ($_FILES['user_image']['name'] != null))
                    {
                        $config['upload_path'] = $this->config->item('user_main_upload_path');
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                        $config['file_name'] = str_replace(' ', '_', $this->input->post('name')) . '_PRO_' . time();
                        $this->upload->initialize($config);

                        if ($this->upload->do_upload('user_image'))
                        {
                            $upload_data = $this->upload->data();
                            //print_r($upload_data);die();
                            $profileimg = $upload_data['file_name'];
                        }
                        else
                        {
                            $this->data['msg'] = array('error' => $this->upload->display_errors());
                        }
                    }
                    $pass = base64_encode($password);
                    $user_data = array(
                        'jeweler_id' => $jeweler_id,
                        'name' => $name,
                        'user_password' => $pass,
                        'user_email' => $email,
                        'user_contact_number' => $contact,
                        'user_image' => $profileimg,
                        'user_device_id' => $device_id,
                        'user_device_type' => $device_type,
                        'user_login_with' => $login_with,
                        'user_active' => 'Enable',
                        'createdby' => $jeweler_id,
                        'createddate' => $date
                    );
                    $result = $this->common->insert_data($user_data, 'user');
                    if ($result == 1)
                    {
                        $user_id = $this->db->insert_id();
                        $user_data = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id,name,user_image,is_loyal');
//                    $app_name = $this->common->get_setting_value(1);
//                    $app_mail = $this->common->get_setting_value(6);
//
//                    $mail = $this->common->get_email_byid(2);
//                    $mailformat = $mail[0]['mailformat'];
//                    $subject = $mail[0]['subject'];
//                    // admin mail
//                    $this->load->library('email');
//                    //Loading E-mail config file
//                    $this->config->load('email', TRUE);
//                    $this->cnfemail = $this->config->item('email');
//                    $this->email->initialize($this->cnfemail);
//                    $this->email->from($app_mail, $app_name);
//                    $this->email->to($app_mail);
//                    $this->email->subject($subject);
//                    $mail_body = str_replace("%firstname%", $name, str_replace("%email%", $email, str_replace("%appname%", $app_name, ($mailformat))));
//                    $this->email->message($mail_body);
//                    $this->email->send();

                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'User registered successfully';
                        $this->data['data'] = $user_data[0];
                        if ($user_data[0]['user_image'] != '')
                        {
                            if (file_exists($this->config->item('user_main_upload_path') . $user_data[0]['user_image']))
                            {
                                $userimage = (($user_data[0]['user_image'] != '') ? base_url() . $this->config->item('user_main_upload_path') . $user_data[0]['user_image'] : base_url() . $this->config->item('noimage'));
                            }
                            else
                            {
                                $userimage = base_url() . $this->config->item('noimage');
                            }

                            $this->data['data']['user_image'] = $userimage;
                        }
                        else
                        {
                            $userimage = base_url() . $this->config->item('noimage');
                            $this->data['data']['user_image'] = $userimage;
                        }
                        if ($user_data[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'Something went wrong. Please try again';
                    }
                }
                // for user image.
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }
        else
        {
            if ($email != '' && $jeweler_id != '' && $login_with != '')
            {
                $result_mail = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
                if ((!empty($result_mail)))
                {
                    $id = $result_mail[0]['user_id'];
                    $response = $this->user_current_status($id);
                    if ($response == "Enable")
                    {
                        $gcmdata = array('user_device_id' => $device_id, 'user_device_type' => $device_type);
                        $this->common->update_data($gcmdata, 'user', 'user_id', $id);
                        $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
//                        echo "<pre>";print_r($login_result);die;
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'You are logged in succesfully';
                        $this->data['data'] = $login_result[0];

                        if ($login_result[0]['user_image'] != '')
                        {
                            if (file_exists($this->config->item('user_main_upload_path') . $login_result[0]['user_image']))
                            {
                                $userimage = (($login_result[0]['user_image'] != '') ? base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'] : base_url() . $this->config->item('noimage'));
                            }
                            else
                            {
                                $userimage = base_url() . $this->config->item('noimage');
                            }

                            $this->data['data']['user_image'] = $userimage;
                        }
                        else
                        {
                            $userimage = base_url() . $this->config->item('noimage');
                            $this->data['data']['user_image'] = $userimage;
                        }

                        if ($login_result[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    elseif ($response == "Disable")
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'You are Disabled.Please contact to admin.';
                    }
                    elseif ($response == "Delete")
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'No user found';
                    }
                    elseif ($response == "Null")
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'Something went wrong. Please try again.';
                    }
                    else
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'Something went wrong. Please try again.';
                    }
                }
                else
                {
                    // for user image.
                    if ((isset($_FILES['user_image']['name'])) && ($_FILES['user_image']['name'] != null))
                    {
                        $config['upload_path'] = $this->config->item('user_main_upload_path');
                        $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
                        $config['file_name'] = str_replace(' ', '_', $this->input->post('name')) . '_PRO_' . time();
                        $this->upload->initialize($config);

                        if ($this->upload->do_upload('user_image'))
                        {
                            $upload_data = $this->upload->data();
                            //print_r($upload_data);die();
                            $profileimg = $upload_data['file_name'];
                        }
                        else
                        {
                            $this->data['msg'] = array('error' => $this->upload->display_errors());
                        }
                    }
                    $pass = base64_encode(uniqid());
                    $user_data = array(
                        'jeweler_id' => $jeweler_id,
                        'name' => $name,
                        'user_password' => $pass,
                        'user_email' => $email,
                        'user_contact_number' => $contact,
                        'user_image' => $profileimg,
                        'user_device_id' => $device_id,
                        'user_device_type' => $device_type,
                        'user_login_with' => $login_with,
                        'user_active' => 'Enable',
                        'createdby' => $jeweler_id,
                        'createddate' => $date
                    );
                    $result = $this->common->insert_data($user_data, 'user');
                    if ($result == 1)
                    {
                        $user_id = $this->db->insert_id();
                        $user_data = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id,name,user_image,is_loyal');
//                    $app_name = $this->common->get_setting_value(1);
//                    $app_mail = $this->common->get_setting_value(6);
//
//                    $mail = $this->common->get_email_byid(2);
//                    $mailformat = $mail[0]['mailformat'];
//                    $subject = $mail[0]['subject'];
//                    // admin mail
//                    $this->load->library('email');
//                    //Loading E-mail config file
//                    $this->config->load('email', TRUE);
//                    $this->cnfemail = $this->config->item('email');
//                    $this->email->initialize($this->cnfemail);
//                    $this->email->from($app_mail, $app_name);
//                    $this->email->to($app_mail);
//                    $this->email->subject($subject);
//                    $mail_body = str_replace("%firstname%", $name, str_replace("%email%", $email, str_replace("%appname%", $app_name, ($mailformat))));
//                    $this->email->message($mail_body);
//                    $this->email->send();

                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'User registered successfully';
                        $this->data['data'] = $user_data[0];
                        if ($user_data[0]['user_image'] != '')
                        {
                            $this->data['data']['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $user_data[0]['user_image'];
                        }

                        if ($user_data[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'Something went wrong. Please try again';
                    }
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }

        echo json_encode($this->data);
        die();
    }

    public function register()
    {
//        echo "hi";die;
//        echo "<pre>";
//        print_r($_POST);
//        die;
        $uuid = trim($this->input->post('jeweler_id'));
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        $name = trim($this->input->post('name'));
        $password = trim($this->input->post('password'));
        $email = trim($this->input->post('email'));
        $contact = trim($this->input->post('contact'));
        $device_id = trim($this->input->post('device_id'));
        $login_with = trim($this->input->post('login_with'));
        $device_type = trim($this->input->post('device_type'));
        $social_id = trim($this->input->post('social_id'));
        $profileimg = '';
        $date = date("Y-m-d H:i:s");

        if ($login_with == "App")
        {
            if ($email != '' && $name != '' && $password != '' && $contact != '' && $jeweler_id != '' && $login_with != '' && $device_type != '')
            {
                $result_mail = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
                if ((!empty($result_mail)))
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'Emailid already exist.';
                }
                else
                {
                    if ((isset($_FILES['user_image']['name'])) && ($_FILES['user_image']['name'] != null))
                    {
                        $config['upload_path'] = $this->config->item('user_main_upload_path');
                        $config['allowed_types'] = '*';
                        $config['file_name'] = str_replace(' ', '_', $this->input->post('name')) . '_PRO_' . time();
                        $this->upload->initialize($config);

                        if ($this->upload->do_upload('user_image'))
                        {
                            $upload_data = $this->upload->data();
                            //print_r($upload_data);die();
                            $profileimg = $upload_data['file_name'];
                        }
                        else
                        {
                            $this->data['msg'] = array('error' => $this->upload->display_errors());
                        }
                    }
                    $pass = base64_encode($password);
                    $user_data = array(
                        'jeweler_id' => $jeweler_id,
                        'name' => $name,
                        'user_password' => $pass,
                        'user_email' => $email,
                        'user_contact_number' => $contact,
                        'user_image' => $profileimg,
                        'user_device_id' => $device_id,
                        'user_device_type' => $device_type,
                        'user_login_with' => $login_with,
                        'user_active' => 'Enable',
                        'createdby' => $jeweler_id,
                        'createddate' => $date
                    );
                    $result = $this->common->insert_data($user_data, 'user');
                    if ($result == 1)
                    {
                        $user_id = $this->db->insert_id();
                        $user_data = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id,name,user_image,is_loyal');
//                    $app_name = $this->common->get_setting_value(1);
//                    $app_mail = $this->common->get_setting_value(6);
//
//                    $mail = $this->common->get_email_byid(2);
//                    $mailformat = $mail[0]['mailformat'];
//                    $subject = $mail[0]['subject'];
//                    // admin mail
//                    $this->load->library('email');
//                    //Loading E-mail config file
//                    $this->config->load('email', TRUE);
//                    $this->cnfemail = $this->config->item('email');
//                    $this->email->initialize($this->cnfemail);
//                    $this->email->from($app_mail, $app_name);
//                    $this->email->to($app_mail);
//                    $this->email->subject($subject);
//                    $mail_body = str_replace("%firstname%", $name, str_replace("%email%", $email, str_replace("%appname%", $app_name, ($mailformat))));
//                    $this->email->message($mail_body);
//                    $this->email->send();

                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'User registered successfully';
                        $this->data['data'] = $user_data[0];
                        if ($user_data[0]['user_image'] != '')
                        {
                            if (file_exists($this->config->item('user_main_upload_path') . $user_data[0]['user_image']))
                            {
                                $userimage = (($user_data[0]['user_image'] != '') ? base_url() . $this->config->item('user_main_upload_path') . $user_data[0]['user_image'] : base_url() . $this->config->item('noimage'));
                            }
                            else
                            {
                                $userimage = base_url() . $this->config->item('noimage');
                            }

                            $this->data['data']['user_image'] = $userimage;
                        }
                        else
                        {
                            $userimage = base_url() . $this->config->item('noimage');
                            $this->data['data']['user_image'] = $userimage;
                        }
                        if ($user_data[0]['is_loyal'] == "Yes")
                        {
                            $this->data['data']['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['data']['is_loyal'] = false;
                        }
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'Something went wrong. Please try again';
                    }
                }
                // for user image.
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }
        else
        {
            if ($social_id != '')
            {
                if ($jeweler_id != '' && $login_with != '')
                {
                    $result_mail = $this->webservice_model->check_user_avalibility_by_social_id($social_id, $jeweler_id);
                    if ((!empty($result_mail)))
                    {
                        $id = $result_mail[0]['user_id'];
                        $response = $this->user_current_status($id);
                        if ($response == "Enable")
                        {
                            $gcmdata = array('user_device_id' => $device_id, 'user_device_type' => $device_type);
                            $this->common->update_data($gcmdata, 'user', 'user_id', $id);
                            $login_result = $this->common->select_database_id('user', 'user_id', $id, 'user_id,name,user_image,is_loyal');
//                        echo "<pre>";print_r($login_result);die;
                            $this->data['status'] = TRUE;
                            $this->data['msg'] = 'You are logged in succesfully';
                            $this->data['data'] = $login_result[0];

                            if ($login_result[0]['user_image'] != '')
                            {
                                if (file_exists($this->config->item('user_main_upload_path') . $login_result[0]['user_image']))
                                {
                                    $userimage = (($login_result[0]['user_image'] != '') ? base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'] : base_url() . $this->config->item('noimage'));
                                }
                                else
                                {
                                    $userimage = base_url() . $this->config->item('noimage');
                                }

                                $this->data['data']['user_image'] = $userimage;
                            }
                            else
                            {
                                $userimage = base_url() . $this->config->item('noimage');
                                $this->data['data']['user_image'] = $userimage;
                            }

                            if ($login_result[0]['is_loyal'] == "Yes")
                            {
                                $this->data['data']['is_loyal'] = true;
                            }
                            else
                            {
                                $this->data['data']['is_loyal'] = false;
                            }
                        }
                        elseif ($response == "Disable")
                        {
                            $this->data["status"] = FALSE;
                            $this->data["msg"] = 'You are Disabled.Please contact to admin.';
                        }
                        elseif ($response == "Delete")
                        {
                            $this->data["status"] = FALSE;
                            $this->data["msg"] = 'No user found';
                        }
                        elseif ($response == "Null")
                        {
                            $this->data["status"] = FALSE;
                            $this->data["msg"] = 'Something went wrong. Please try again.';
                        }
                        else
                        {
                            $this->data["status"] = FALSE;
                            $this->data["msg"] = 'Something went wrong. Please try again.';
                        }
                    }
                    else
                    {
                        if ($email != '')
                        {
                            $result_email = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
                            if ((!empty($result_email)))
                            {
                                $this->data['status'] = FALSE;
                                $this->data['msg'] = 'Emailid already exist.';
                            }
                            else
                            {

//                            echo "<pre>";print_r($_POST);die;
                                // for user image.
                                if ((isset($_FILES['user_image']['name'])) && ($_FILES['user_image']['name'] != null))
                                {
                                    $config['upload_path'] = $this->config->item('user_main_upload_path');
                                    $config['allowed_types'] = '*';
                                    $config['file_name'] = str_replace(' ', '_', $this->input->post('name')) . '_PRO_' . time();
                                    $this->upload->initialize($config);

                                    if ($this->upload->do_upload('user_image'))
                                    {
                                        $upload_data = $this->upload->data();
                                        //print_r($upload_data);die();
                                        $profileimg = $upload_data['file_name'];
                                    }
                                    else
                                    {
                                        $this->data['msg'] = array('error' => $this->upload->display_errors());
                                    }
                                }
                                $pass = base64_encode(uniqid());
                                $user_data = array(
                                    'jeweler_id' => $jeweler_id,
                                    'name' => $name,
                                    'user_password' => $pass,
                                    'user_email' => $email,
                                    'user_contact_number' => $contact,
                                    'user_image' => $profileimg,
                                    'user_device_id' => $device_id,
                                    'user_device_type' => $device_type,
                                    'user_login_with' => $login_with,
                                    'user_social_id' => $social_id,
                                    'user_active' => 'Enable',
                                    'createdby' => $jeweler_id,
                                    'createddate' => $date
                                );
                                $result = $this->common->insert_data($user_data, 'user');
                                if ($result == 1)
                                {
                                    $user_id = $this->db->insert_id();
                                    $user_data = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id,name,user_image,is_loyal');
//                    $app_name = $this->common->get_setting_value(1);
//                    $app_mail = $this->common->get_setting_value(6);
//
//                    $mail = $this->common->get_email_byid(2);
//                    $mailformat = $mail[0]['mailformat'];
//                    $subject = $mail[0]['subject'];
//                    // admin mail
//                    $this->load->library('email');
//                    //Loading E-mail config file
//                    $this->config->load('email', TRUE);
//                    $this->cnfemail = $this->config->item('email');
//                    $this->email->initialize($this->cnfemail);
//                    $this->email->from($app_mail, $app_name);
//                    $this->email->to($app_mail);
//                    $this->email->subject($subject);
//                    $mail_body = str_replace("%firstname%", $name, str_replace("%email%", $email, str_replace("%appname%", $app_name, ($mailformat))));
//                    $this->email->message($mail_body);
//                    $this->email->send();

                                    $this->data['status'] = TRUE;
                                    $this->data['msg'] = 'User registered successfully';
                                    $this->data['data'] = $user_data[0];

                                    if (file_exists($this->config->item('user_main_upload_path') . $user_data[0]['user_image']))
                                    {
                                        $userimage = (($user_data[0]['user_image'] != '') ? base_url() . $this->config->item('user_main_upload_path') . $user_data[0]['user_image'] : base_url() . $this->config->item('noimage'));
                                    }
                                    else
                                    {
                                        $userimage = base_url() . $this->config->item('noimage');
                                    }
                                    $this->data['data']['user_image'] = $userimage;

                                    if ($user_data[0]['is_loyal'] == "Yes")
                                    {
                                        $this->data['data']['is_loyal'] = true;
                                    }
                                    else
                                    {
                                        $this->data['data']['is_loyal'] = false;
                                    }
                                }
                                else
                                {
                                    $this->data['status'] = FALSE;
                                    $this->data['msg'] = 'Something went wrong. Please try again';
                                }
                            }
                        }
                        else
                        {
                            $this->data['status'] = FALSE;
                            $this->data['msg'] = 'Email is required';
                        }
                    }
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'All fields required';
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }

        echo json_encode($this->data);
        die();
    }

    // user forget password
    public function forgot_password()
    {
        $email = $this->input->post('email');
        $uuid = $this->input->post('jeweler_id');
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        if ($email != '' && $jeweler_id != '')
        {
            $result_mail = $this->webservice_model->check_user_avalibility($email, $jeweler_id);
            if (empty($result_mail))
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'No such mail id is available';
            }
            else
            {
                $userdata = $result_mail;
//                  echo "<pre>";print_r($userdata);die();
                $name = $userdata[0]['name'];
                $user_id = $userdata[0]['user_id'];
                $app_name = $this->common->get_setting_value(1);
                $app_mail = $this->common->get_setting_value(6);
                $mailformat = "<p>Hello&nbsp; " . $userdata[0]['name'] . ",</p>
                                       <p>
                                        <span>Your login credentials are as below. </span></p>
                                        <table>
                                        <tbody>
                                        <tr>
                                        <td> Email</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>" . $userdata[0]['user_email'] . "</td>
                                        </tr>
                                        
                                        <tr>
                                        <td> Password</td>
                                        <td>&nbsp;:&nbsp;</td>
                                        <td>" . base64_decode($userdata[0]['user_password']) . "</td>
                                        </tr>
                                        
                                        </tbody>
                                        </table>
                                        <p>&nbsp;</p>
                                        <p>Regards,<br/>
                                        " . $app_name . " Team.</p>";


                $subject = $app_name . ': Forgot Password';
                $this->load->library('email');
                //Loading E-mail config file
                $this->config->load('email', TRUE);
                $this->cnfemail = $this->config->item('email');
                $this->email->initialize($this->cnfemail);
                $this->email->from($app_mail, $app_name);
                $this->email->to($userdata[0]['user_email']);
                $this->email->subject($subject);
                $mail_body = str_replace("%email%", $email, str_replace("%appname%", $app_name, ($mailformat)));
                $this->email->message($mail_body);
//                echo "<pre>";
//                print_r($mail_body);
//                exit;
//                
                //Sending mail to admin
                if ($this->email->send())
                {
                    $this->data["status"] = TRUE;
                    $this->data["msg"] = "Your password has been sent successfully to your email.";
                }
                else
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = "Somthing went to wrong please try again.";
                }
            }
        }
        elseif ($email != '' && $jeweler_id == '')
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'Something went wrong.Please try again';
        }
        else
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'email is required';
        }
        echo json_encode($this->data);
        die();
    }

    // get product image
    function get_product_image($product_id)
    {
        $product_images = $this->webservice_model->get_all_product_images($product_id);
        return $product_images;
    }

    // get product image
    function get_product_diamond_image($product_id)
    {
        $product_diamond_images = $this->webservice_model->get_all_product_diamond_images($product_id);
        return $product_diamond_images;
    }

    // get product size
    function get_product_size($product_id)
    {
        $product_size = $this->webservice_model->get_all_product_size($product_id);
        return $product_size;
    }

    // get product diamond attributes
    function get_product_diamond_attributes($product_id)
    {
        $product_diamond_attribute = $this->webservice_model->get_all_product_diamond_attributes($product_id);
        return $product_diamond_attribute;
    }

    public function get_jeweler_id_from_uuid($uuid = '')
    {
        $jeweler_id = '';
//        return $uuid;die;
        $jeweler_data = $this->common->select_database_id('jeweler', 'jeweler_UUID', $uuid, $data = 'jeweler_id');
//        echo $this->db->last_query();   die;
        if (!empty($jeweler_data))
        {
            return $jeweler_id = $jeweler_data[0]['jeweler_id'];
        }
        else
        {
            return $jeweler_id;
        }
    }

    // get all product which are bind with beacon
    public function get_beacon_data()
    {
        $jeweler_id = '';
        $user_id = '';
        $uuid = $this->input->post('uuid');
        $user_id = $this->input->post('user_id');
        //$user_id = 6;
//        $jeweler_id = 7;
        if ($uuid != '')
        {
            $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
            if ($jeweler_id != '' && $jeweler_id != '0')
            {
                if ($user_id != '')
                {
                    $wishlist_product = $this->common->select_database_id('product_wishlist', 'user_id', $user_id, $data = 'pro_id');
                    $wishlist_product_id = iterator_to_array(new RecursiveIteratorIterator(new RecursiveArrayIterator($wishlist_product)), 0);
                    $user_data = $this->check_is_loyal($user_id);
                    if ($user_data != 'NoUser')
                    {
                        if ($user_data == "Yes")
                        {
                            $this->data['is_loyal'] = true;
                        }
                        else
                        {
                            $this->data['is_loyal'] = false;
                        }
                    }
                }
                else
                {
                    $wishlist_product_id = array();
                    $this->data['is_loyal'] = false;
                }
//            echo "<pre>";print_r($wishlist_product_id);die;
                $final_array = array();
                $result = $this->webservice_model->get_all_beacon($jeweler_id);
                $result_product = $this->webservice_model->get_all_product_beacon($jeweler_id);

                if (!empty($result))
                {
                    for ($i = 0; $i < count($result); $i++)
                    {
                        $department = $this->webservice_model->get_all_department($result[$i]['dept_id']);
                        $product = $this->webservice_model->get_all_product($result[$i]['dept_id']);
                        unset($result[$i]['dept_id']);
                        for ($j = 0; $j < count($product); $j++)
                        {
//                          echo "<pre>";print_r($wishlist_product_id);die;
                            $pro_id = $product[$j]['pro_id'];
                            if (in_array($pro_id, $wishlist_product_id))
                            {
                                $product[$j]['is_favourite'] = TRUE;
                            }
                            else
                            {
                                $product[$j]['is_favourite'] = FALSE;
                            }

                            $pro_image = $this->get_product_image($pro_id);
                            $pro_diamond_image = $this->get_product_diamond_image($pro_id);
                            $product_size = $this->get_product_size($pro_id);
                            $product_diamond_attribute = $this->get_product_diamond_attributes($pro_id);
                            $product[$j]['pro_default_image'] = $pro_image[0]['pro_image'];
                            $product[$j]['pro_image'] = $pro_image;
                            $product[$j]['pro_attributes'] = array();
                            $product[$j]['pro_size'] = $product_size;
                            if ($product[$j]['pro_has_diamond'] == "Yes")
                            {
                                unset($product[$j]['pro_has_diamond']);
                                $product[$j]['pro_has_diamond'] = TRUE;
                                $product[$j]['diamond']['pro_diamond_name'] = $product[$j]['pro_diamond_name'];
                                $product[$j]['diamond']['pro_diamond_code'] = $product[$j]['pro_diamond_code'];
                                $product[$j]['diamond']['pro_diamond_attributes'] = $product_diamond_attribute;
                                $product[$j]['diamond']['pro_diamond_image'] = $pro_diamond_image;
                            }
                            else
                            {
                                unset($product[$j]['pro_has_diamond']);
                                $product[$j]['pro_has_diamond'] = FALSE;
                                $product[$j]['diamond'] = NULL;
                            }

                            unset($product[$j]['pro_diamond_name']);
                            unset($product[$j]['pro_diamond_code']);
                        }
                        $result[$i]['department'] = $department[0];
                        $result[$i]['department']['product_data'] = $product;
                        $result[$i]['product'] = array();
                    }
                }
                $final_array = $result;
                if (!empty($result_product))
                {
                    for ($i = 0; $i < count($result_product); $i++)
                    {
                        $product = $this->webservice_model->get_product($result_product[$i]['pro_id']);
                        $pro_image = $this->get_product_image($result_product[$i]['pro_id']);
                        $pro_diamond_image = $this->get_product_diamond_image($result_product[$i]['pro_id']);
                        $product_size = $this->get_product_size($result_product[$i]['pro_id']);
                        $product_diamond_attribute = $this->get_product_diamond_attributes($result_product[$i]['pro_id']);

                        for ($p = 0; $p < count($product); $p++)
                        {
                            if (in_array($product[$p]['pro_id'], $wishlist_product_id))
                            {
                                $product[$p]['is_favourite'] = TRUE;
                            }
                            else
                            {
                                $product[$p]['is_favourite'] = FALSE;
                            }
                            $product[$p]['pro_default_image'] = $pro_image[0]['pro_image'];
                            $product[$p]['pro_image'] = $pro_image;
                            $product[$p]['pro_attributes'] = array();
                            $product[$p]['pro_size'] = $product_size;
                            if ($product[$p]['pro_has_diamond'] == "Yes")
                            {
                                unset($product[$p]['pro_has_diamond']);
                                $product[$p]['pro_has_diamond'] = TRUE;
                                $product[$p]['diamond']['pro_diamond_name'] = $product[$p]['pro_diamond_name'];
                                $product[$p]['diamond']['pro_diamond_code'] = $product[$p]['pro_diamond_code'];
                                $product[$p]['diamond']['pro_diamond_attributes'] = $product_diamond_attribute;
                                $product[$p]['diamond']['pro_diamond_image'] = $pro_diamond_image;
                            }
                            else
                            {
                                unset($product[$p]['pro_has_diamond']);
                                $product[$p]['pro_has_diamond'] = FALSE;
                                $product[$p]['diamond'] = NULL;
                            }

                            unset($product[$p]['pro_diamond_name']);
                            unset($product[$p]['pro_diamond_code']);
                        }
                        $result_product[$i]['department'] = NULL;
                        $result_product[$i]['product'] = $product;
                        unset($result_product[$i]['pro_id']);
                    }
                }
                $final_array = array_merge($final_array, $result_product);
                $this->data["status"] = TRUE;
                $this->data['jeweler_data'] = $final_array;
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'Please enter proper UUID.';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'Something went wrong. Please try again';
        }
        echo json_encode($this->data);
        die();
    }

    // add or remove from wishlist.
    public function wishlist()
    {
        $type = '';
        $pro_id = $this->input->post('pro_id');
        $user_id = $this->input->post('user_id');
        $type = $this->input->post('type');

        $wishlist_data = array(
            'pro_id' => $pro_id,
            'user_id' => $user_id
        );
        if ($user_id != '')
        {
            if ($type == '1')
            {
                if ($user_id != '' && $pro_id != '')
                {
                    $wishlist_result = $this->common->select_database_by_muliple_where('product_wishlist', $wishlist_data, '*');
                    if (!empty($wishlist_result))
                    {
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'Product already added in wishlist.';
                    }
                    else
                    {
                        $result = $this->common->insert_data($wishlist_data, 'product_wishlist');
                        if ($result)
                        {
                            $this->data['status'] = TRUE;
                            $this->data['msg'] = 'Product has been added to wishlist successfully';
                        }
                        else
                        {
                            $this->data['status'] = FALSE;
                            $this->data['msg'] = 'Something went wrong. Please try again.';
                        }
                    }
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'All fields required';
                }
            }
            elseif ($type == '0')
            {
                if ($user_id != '' && $pro_id != '')
                {
                    $result = $this->common->delete_multiple_id('product_wishlist', $wishlist_data);
                    if ($result)
                    {
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'Product has been removed from wishlist successfully';
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'Something went wrong. Please try again.';
                    }
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'All fields required';
                }
            }
            else
            {
                $wishlist_result = $this->webservice_model->get_wishlist_data($user_id);
                if (!empty($wishlist_result))
                {
                    for ($i = 0; $i < count($wishlist_result); $i++)
                    {
                        $pro_image = $this->get_product_image($wishlist_result[$i]['pro_id']);
                        $wishlist_result[$i]['pro_default_image'] = $pro_image[0]['pro_image'];
                    }
                    $this->data['status'] = TRUE;
                    $this->data['product_data'] = $wishlist_result;
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'No records';
                }
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'All fields required';
        }
        echo json_encode($this->data);
        die();
    }

    function demo_cart()
    {
        $final_array = array();

        //Product 1
        $final_array['product_detail'][0]['pro_id'] = 6;
        $final_array['product_detail'][0]['pro_quantity'] = 52;
        $final_array['product_detail'][0]['pro_size'] = 4;
        $final_array['product_detail'][0]['pro_price'] = 100;

        //Product 2
        $final_array['product_detail'][1]['pro_id'] = 7;
        $final_array['product_detail'][1]['pro_quantity'] = 51;
        $final_array['product_detail'][1]['pro_size'] = 101;
        $final_array['product_detail'][1]['pro_price'] = 101;
        //Product 3
        $final_array['product_detail'][2]['pro_id'] = 8;
        $final_array['product_detail'][2]['pro_quantity'] = 59;
        $final_array['product_detail'][2]['pro_size'] = 102;
        $final_array['product_detail'][2]['pro_price'] = 102;

        return (json_encode($final_array));
    }

    public function check_product_size($pro_id, $size)
    {
        $result = $this->common->select_database_by_muliple_where('product_size', array('pro_id' => $pro_id, 'pro_size' => $size), 'pro_size');
        if (!empty($result))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function manage_cart()
    {
        $product_array = array();
        $cart_data = $this->input->post('cart_data');
        $user_id = $this->input->post('user_id');
        $cart_data = (base64_decode($cart_data));
        $cart_data = json_decode($cart_data);
//        $cart_data = json_decode($this->demo_cart());
//        echo "<pre>";
//        print_r($cart_data);
//        die;
        if (isset($user_id) && $user_id != '')
        {
            if (isset($cart_data))
            {
                if (!empty($cart_data))
                {
                    if (isset($cart_data))
                    {
                        $product_data = $cart_data->product_detail;
                        for ($i = 0; $i < count($product_data); $i++)
                        {
                            if (isset($product_data[$i]->pro_id) && isset($product_data[$i]->pro_quantity) && isset($product_data[$i]->pro_size) && isset($product_data[$i]->pro_price))
                            {
                                $pro_id = $product_data[$i]->pro_id;
                                $pro_quantity = $product_data[$i]->pro_quantity;
                                $pro_size = $product_data[$i]->pro_size;
                                $pro_price = $product_data[$i]->pro_price;

                                if ($pro_id != '' && $pro_id != 0)
                                {
                                    $product = $this->common->select_database_id('product', 'pro_id', $pro_id, "*");
                                    if (!empty($product))
                                    {
                                        $pro_images = $this->get_product_image($pro_id);
                                        $product_size = $this->check_product_size($pro_id, $pro_size);

                                        if ($product_size)
                                        {
                                            $offer = $this->webservice_model->get_special_offer($user_id);
                                            $loyal_user = $this->check_is_loyal($user_id);
                                            if ($loyal_user == "Yes")
                                            {
                                                if (!empty($offer))
                                                {
                                                    $percent = $offer[0]['percentage'];
                                                    $percent_price = ( $product[0]['pro_loyal_price'] * $percent ) / 100;
                                                    $price = ( $product[0]['pro_loyal_price'] - $percent_price );
                                                }
                                                else
                                                {
                                                    $price = $product[0]['pro_loyal_price'];
                                                }
                                            }
                                            else
                                            {
                                                if (!empty($offer))
                                                {
                                                    $percent = $offer[0]['percentage'];
                                                    $percent_price = ( $product[0]['pro_price'] * $percent ) / 100;
                                                    $price = ( $product[0]['pro_price'] - $percent_price );
                                                }
                                                else
                                                {
                                                    $price = $product[0]['pro_price'];
                                                }
                                            }
                                            $product_array['product_detail'][$i]['pro_id'] = $pro_id;
                                            $product_array['product_detail'][$i]['pro_name'] = $product[0]['pro_name'];
                                            $product_array['product_detail'][$i]['pro_quantity'] = $pro_quantity;
                                            $product_array['product_detail'][$i]['pro_size'] = $pro_size;
                                            $product_array['product_detail'][$i]['pro_price'] = $price;
                                            if (!empty($pro_images))
                                            {
                                                $product_array['product_detail'][$i]['pro_image'] = $pro_images[0]['pro_image'];
                                            }
                                            else
                                            {
                                                $product_array['product_detail'][$i]['pro_image'] = base_url() . $this->config->item('noimage');
                                            }
                                            if ($product[0]['pro_active'] == "Delete")
                                            {
                                                $product_array['product_detail'][$i]['is_delete'] = true;
                                            }
                                            else
                                            {
                                                $product_array['product_detail'][$i]['is_delete'] = false;
                                            }
                                        }
                                        else
                                        {
                                            $product_array['product_detail'][$i]['pro_id'] = $pro_id;
                                            $product_array['product_detail'][$i]['pro_quantity'] = $pro_quantity;
                                            $product_array['product_detail'][$i]['pro_size'] = $pro_size;
                                            $product_array['product_detail'][$i]['pro_price'] = '';
                                            $product_array['product_detail'][$i]['pro_image'] = '';
                                            $product_array['product_detail'][$i]['is_delete'] = true;
                                        }
//                                    print_r($pro_images);die;
                                    }
                                    else
                                    {
                                        $product_array['product_detail'][$i]['pro_id'] = $pro_id;
                                        $product_array['product_detail'][$i]['pro_quantity'] = $pro_quantity;
                                        $product_array['product_detail'][$i]['pro_size'] = $pro_size;
                                        $product_array['product_detail'][$i]['pro_price'] = '';
                                        $product_array['product_detail'][$i]['pro_image'] = '';
                                        $product_array['product_detail'][$i]['is_delete'] = true;
                                    }
                                }
                            }
                            else
                            {
                                $this->data["status"] = FALSE;
                                $this->data["msg"] = 'Something went wrong. Please try again.';
                            }
                        }
                        $this->data["status"] = TRUE;
                        $this->data["msg"] = 'Success';
                        $this->data["data"] = $product_array;
                    }
                    else
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'Something went wrong. Please try again.';
                    }
                }
                else
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'Something went wrong. Please try again.';
                }
            }
            else
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'Something went wrong. Please try again.';
            }
        }
        else
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'User id required. Please try again.';
        }
        echo json_encode($this->data);
        die();
    }

    // add product order 
    public function add_order()
    {
        $order_data = $this->input->post('order_data');
        $order_data = (base64_decode($order_data));
        $order_data = json_decode($order_data);
//        $order_data = json_decode($this->demo_cart());
//        print_r($order_data[0]->product_detail);
//        die;
//        echo $order_data[0]->product_detail;
//        die;
        if (isset($order_data))
        {
            if (!empty($order_data))
            {
                if (isset($order_data[0]->order_detail->user_id) && isset($order_data[0]->order_detail->jeweler_id) && isset($order_data[0]->order_detail->transactionid) && isset($order_data[0]->order_detail->order_amount) && isset($order_data[0]->order_detail->payment_mode) && isset($order_data[0]->order_detail->payment_status) && isset($order_data[0]->product_detail))
                {
                    $user_id = $order_data[0]->order_detail->user_id;
                    $jeweler_uuid = $order_data[0]->order_detail->jeweler_id;
                    $jeweler_id = $this->get_jeweler_id_from_uuid($jeweler_uuid);
                    $order_amount = $order_data[0]->order_detail->order_amount;
                    $transactionid = $order_data[0]->order_detail->transactionid;
                    $payment_mode = $order_data[0]->order_detail->payment_mode;
                    $payment_status = $order_data[0]->order_detail->payment_status;
                    $product_data = $order_data[0]->product_detail;
                    if ($payment_status == "1")
                    {
                        $payment_status = 'Success';
                    }
                    else
                    {
                        $payment_status = 'Fail';
                    }
                    if (isset($order_data[0]->payment_type))
                    {
                        $payment_type = $order_data[0]->payment_type;
                    }
                    else
                    {
                        $payment_type = '';
                    }
                    $orderdata = array(
                        'user_id' => $user_id,
                        'jeweler_id' => $jeweler_id,
                        'order_amount' => $order_amount,
                        'order_transactionid' => $transactionid,
                        'order_payment_mode' => $payment_mode,
                        'order_status' => 'Pending',
                        'order_payment_status' => $payment_status,
                        'payment_type' => $payment_type,
                        'createddate' => date('Y-m-d H:i:s')
                    );
                    $result = $this->common->insert_data($orderdata, 'jeweler_order');
                    if ($result)
                    {
                        $order_id = $this->db->insert_id();
                        for ($i = 0; $i < count($product_data); $i++)
                        {
                            if (isset($product_data[$i]->pro_id) && isset($product_data[$i]->pro_quantity) && isset($product_data[$i]->pro_size) && isset($product_data[$i]->pro_price))
                            {
                                $pro_id = $product_data[$i]->pro_id;
                                $pro_quantity = $product_data[$i]->pro_quantity;
                                $pro_size = $product_data[$i]->pro_size;
                                $pro_price = $product_data[$i]->pro_price;
                                $productdata = array(
                                    'order_id' => $order_id,
                                    'pro_id' => $pro_id,
                                    'order_pro_quantity' => $pro_quantity,
                                    'order_pro_size' => $pro_size,
                                    'order_pro_price' => $pro_price
                                );
                                $this->common->insert_data($productdata, 'jeweler_order_product');
                            }
                            else
                            {
                                $this->data["status"] = FALSE;
                                $this->data["msg"] = 'Something went wrong. Please try again.';
                            }
                        }
                        $this->data["status"] = TRUE;
                        $this->data["msg"] = 'Your order has been placed successfully';
                    }
                    else
                    {
                        $this->data["status"] = FALSE;
                        $this->data["msg"] = 'Something went wrong. Please try again.';
                    }
                }
                else
                {
                    $this->data["status"] = FALSE;
                    $this->data["msg"] = 'Something went wrong. Please try again.';
                }
            }
            else
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'Something went wrong. Please try again.';
            }
        }
        else
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'Something went wrong. Please try again.';
        }
        echo json_encode($this->data);
        die();
    }

    public function order()
    {
        $user_id = $this->input->post('user_id');
        if ($user_id != '' && $user_id != 0)
        {
            $order_data = $this->common->select_database_id('jeweler_order', 'user_id', $user_id, $data = 'order_id,order_transactionid as transactionid,order_amount as amount,order_payment_mode as payment_mode,order_status,payment_type,order_payment_status as payment_status,createddate as transactiondate');
            $this->data['data'] = $order_data;
            $this->data["status"] = TRUE;
            $this->data["msg"] = "Success";
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'Something went wrong. Please try again';
        }
        echo json_encode($this->data);
        die();
    }

    // send notification 
    public function send_notification($registatoin_ids, $message)
    {
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';

        $fields = array(
            'registration_ids' => $registatoin_ids,
            'data' => $message,
        );

        $headers = array(
            'Authorization: key=AIzaSyBAdLjaJU0dbcrAAx9QkHmYUqM5U90s-Fk',
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE)
        {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        return $result;
    }

    public function logout()
    {
        $user_id = $this->input->post('user_id');
        if ($user_id != '' && $user_id != 0)
        {
            $result = $this->common->update_data(array('gcmreg_id' => '0'), 'user', 'user_id', (int) $user_id);

            if ($result)
            {
                $this->data["status"] = TRUE;
                $this->data["msg"] = 'Logout successfully';
            }
            else
            {
                $this->data["status"] = FALSE;
                $this->data["msg"] = 'Something went wrong. Please try again';
            }
        }
        else
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'Something went wrong. Please try again';
        }
        echo json_encode($this->data);
        die();
    }

    public function user_current_status($user_id)
    {
//        echo $user_id;die;
        $userdata = $this->common->select_database_id('user', 'user_id', $user_id, "user_active");
//        echo $this->db->last_query();
//        echo "<pre>";print_r($userdata);die;
        if (count($userdata) > 0)
        {
            if ($userdata[0]['user_active'] == "Delete")
            {
                return "Deleted";
            }
            elseif ($userdata[0]['user_active'] == "Enable")
            {
                return "Enable";
            }
            elseif ($userdata[0]['user_active'] == "Disable")
            {
                return "Disable";
            }
            else
            {
                return "Null";
            }
        }
        else
        {
            return "NoRecord";
        }
    }

    // View User Profile
    public function my_profile()
    {
        $user_id = $this->input->post('user_id');
        $response = $this->user_current_status($user_id);
//        die;
        if ($response == "Enable")
        {
            if ($user_id != '')
            {
                $login_result = $this->common->selectRecords($table = 'user', $fields = array('user_id', 'name', 'user_email', 'user_contact_number', 'user_image'), $condition = array('user_id' => $user_id, 'user_active !=' => 'Delete'), $resulttype = 'result_array');
                //              echo $this->db->last_query();die();
//                echo "<pre>";print_r($login_result);die;
                if (!empty($login_result))
                {

                    if ($login_result[0]['user_image'] != '')
                    {
                        $login_result[0]['user_image'] = base_url() . $this->config->item('user_main_upload_path') . $login_result[0]['user_image'];
                    }

                    $this->data['status'] = TRUE;
                    $this->data['msg'] = 'success';
                    $this->data['data'] = $login_result[0];
                }
                else
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'No record found';
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }
        elseif ($response == "Disable")
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'You are Disabled from admin';
        }
        elseif ($response == "Deleted")
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'No user found';
        }
        elseif ($response == "Null")
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'Something went wrong. Please try again.';
        }
        else
        {
            $this->data["status"] = FALSE;
            $this->data["msg"] = 'Something went wrong. Please try again.';
        }
        echo json_encode($this->data);
        die();
    }

    // Edit User profile 
    public function edit_profile()
    {
        $user_id = $this->input->post('user_id');
        $user_name = $this->input->post('name');
        $user_email = $this->input->post('user_email');
        $user_contact_number = $this->input->post('user_contact_number');
        $date = date("Y-m-d H:i:s");
        $profileimg = '';


        if (isset($user_id) && isset($user_name) && isset($user_email) && isset($user_contact_number))
        {
            if ($user_id != '' && $user_name != '' && $user_email != '' && $user_contact_number != '')
            {
//                $result_mail = $this->common->select_database_id('user', 'user_email', $user_email, '');
                $result_mail = $this->common->select_database_by_muliple_where('user', array('user_email' => $user_email, 'user_id !=' => $user_id), $data = '*');
                if ((!empty($result_mail)))
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'Email already Exist';
                }
                else
                {
                    if ((isset($_FILES['user_image']['name'])) && ($_FILES['user_image']['name'] != null))
                    {
                        $config['upload_path'] = $this->config->item('user_main_upload_path');
                        $config['allowed_types'] = '*';
                        $config['file_name'] = str_replace(' ', '_', $this->input->post('name')) . '_PRO_' . time();
                        $this->upload->initialize($config);

                        if ($this->upload->do_upload('user_image'))
                        {
                            $upload_data = $this->upload->data();
                            //print_r($upload_data);die();
                            $profileimg = $upload_data['file_name'];
                        }
                        else
                        {
                            $this->data['error'] = array('error' => $this->upload->display_errors());
                        }
                    }

                    if ($profileimg != '')
                    {
                        $user_data = array(
                            'name' => $user_name,
                            'user_email' => $user_email,
                            'user_contact_number' => $user_contact_number,
                            'user_image' => $profileimg,
                            'modifiedby' => $user_id,
                            'modifieddate' => $date
                        );
                    }
                    else
                    {
                        $user_data = array(
                            'name' => $user_name,
                            'user_email' => $user_email,
                            'user_contact_number' => $user_contact_number,
                            'modifiedby' => $user_id,
                            'modifieddate' => $date
                        );
                    }

                    $result = $this->webservice_model->editprofile('user', $user_data, 'user_id', $user_id);
                    if ($result)
                    {
                        $u_data = array('user_id' => $user_id, 'name' => $user_name);
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'Profile updated successfully';
                        $this->data['data'] = $u_data;
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'something went wrong. Please try again';
                    }
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'All fields required';
        }
        echo json_encode($this->data);
        die();
    }

    // Edit User profile 
    public function change_password()
    {
        $user_id = $this->input->post('user_id');
        $old_pass = $this->input->post('old_pass');
        $new_pass = $this->input->post('new_pass');
        $date = date("Y-m-d H:i:s");
        //print_r($_POST);die;
        if (isset($user_id) && isset($old_pass) && isset($new_pass))
        {
            if ($user_id != '' && $old_pass != '' && $new_pass != '')
            {
                $result_data = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id,name,user_password,user_active');
                if ((empty($result_data)))
                {
                    $this->data['status'] = FALSE;
                    $this->data['msg'] = 'Something went wrong. Please try again.';
                }
                else
                {
                    if ($result_data[0]['user_password'] == base64_encode($old_pass))
                    {
                        $user_data = array(
                            'user_password' => base64_encode($new_pass),
                            'modifiedby' => $user_id,
                            'modifieddate' => $date
                        );
                        $result = $this->common->update_data($user_data, 'user', 'user_id', (int) $user_id);
                        if ($result)
                        {
                            $u_data = array('user_id' => $user_id, 'name' => $result_data[0]['name']);
                            $this->data['status'] = TRUE;
                            $this->data['msg'] = 'Password has been updated successfully';
                            $this->data['data'] = $u_data;
                        }
                        else
                        {
                            $this->data['status'] = FALSE;
                            $this->data['msg'] = 'something went wrong. Please try again.';
                        }
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'Old password is wrong.Please try again.';
                    }
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'All fields required';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'All fields required';
        }
        echo json_encode($this->data);
        die();
    }

    public function page()
    {
        $page_id = $this->input->post('page_id');
        $uuid = $this->input->post('jeweler_id');
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        if ($page_id != "" && $jeweler_id != '')
        {
            $page_content = $this->common->select_database_by_muliple_where('page', array('master_page_id' => $page_id, 'jeweler_id' => $jeweler_id), 'page_id,title,content');
            if (!empty($page_content))
            {
                $this->data['status'] = TRUE;
                $this->data['msg'] = 'success';
                $this->data['data'] = $page_content;
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'Page not found';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'Something went wrong.Please try again';
        }
        echo json_encode($this->data);
        die();
    }

//    public function advertisement1($jeweler_id = '')
//    {
//        if ($jeweler_id == "")
//        {
//            $uuid = $this->input->post('jeweler_id');
//            $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
//        }
//        $adv_data = $this->webservice_model->get_advertise1();
//        if (!empty($adv_data))
//        {
//            if ($adv_data[0]['jeweler_add_active'] == "Enable")
//            {
//                $this->data['status'] = TRUE;
//                $this->data['data']['adv_id'] = $adv_data[0]['jeweler_adv_id'];
//                $this->data['data']['text'] = $adv_data[0]['jeweler_adv_text'];
//                $this->data['data']['url'] = $adv_data[0]['jeweler_adv_url'];
//                $this->data['data']['image'] = base_url() . $this->config->item('advretise_main_upload_path') . $adv_data[0]['jeweler_adv_image'];
//            }
//            else
//            {
//                $this->advertisement($jeweler_id);
//            }
//        }
//        else
//        {
//            $this->data['status'] = FALSE;
//            $this->data['data'] = array();
//        }
//        echo json_encode($this->data);
//        die();
//    }

    public function advertisement($jeweler_id = '')
    {
        if ($jeweler_id == "")
        {
            $user_id = $this->input->post('user_id');
            $uuid = $this->input->post('jeweler_id');
            $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        }
        $adv_data = $this->webservice_model->get_advertise();

        if ($user_id != '')
        {
            if (!empty($adv_data))
            {
                $adv_impression_data = array(
                    'user_id' => $user_id,
                    'jeweler_adv_id' => $adv_data[0]['jeweler_adv_id'],
                    'click' => '0',
                    'createddate' => date('Y-m-d H:i:s')
                );
                $this->common->insert_data($adv_impression_data, 'jeweler_adv_impression');
                $id = $this->db->insert_id();
                $update_total_impression = $adv_data[0]['jeweler_add_total_impression'] + 1;
                $advertise_data = array(
                    'jeweler_add_total_impression' => $update_total_impression
                );
                $jeweler_add_id = $adv_data[0]['jeweler_add_id'];
                $res = $this->common->update_data($advertise_data, 'jeweler_advertise', 'jeweler_add_id', (int) $jeweler_add_id);
                $this->data['status'] = TRUE;
                $this->data['data']['adv_id'] = $id;
                $this->data['data']['adv_click_id'] = $adv_data[0]['jeweler_add_id'];
                $this->data['data']['text'] = $adv_data[0]['jeweler_adv_text'];
                $this->data['data']['url'] = $adv_data[0]['jeweler_adv_url'];
                $this->data['data']['image'] = $adv_data[0]['jeweler_adv_image'];
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['data'] = array();
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'userid required';
        }
        echo json_encode($this->data);
        die();
    }

    public function add_click()
    {
        $adv_id = $this->input->post('adv_id');
        $adv_click_id = $this->input->post('adv_click_id');
        $advertise_click_data = array(
            'click' => '1'
        );
        $res = $this->common->update_data($advertise_click_data, 'jeweler_adv_impression', 'jeweler_adv_imp_id', (int) $adv_id);
        if ($res)
        {
            $total_click_data = $this->common->select_database_id('jeweler_advertise', 'jeweler_add_id', $adv_click_id, 'jeweler_add_total_click');
            if (!empty($total_click_data))
            {
                $total_click = $total_click_data[0]['jeweler_add_total_click'] + 1;
                $advertise_data = array(
                    'jeweler_add_total_click' => $total_click
                );
                $this->common->update_data($advertise_data, 'jeweler_advertise', 'jeweler_add_id', (int) $adv_click_id);
            }
            $this->data['status'] = TRUE;
        }
        else
        {
            $this->data['status'] = FALSE;
        }
        echo json_encode($this->data);
        die();
    }

    public function check_user_availibility($user_id)
    {
        $result = $this->common->select_database_id('user', 'user_id', $user_id, 'user_id');
        if (count($result) > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public function user_inside_store()
    {
        $user_id = $this->input->post('user_id');
        $type = $this->input->post('type');
        $date = date('Y-m-d');
        if ($user_id != '')
        {
            $user_inside = $this->common->select_database_by_muliple_where('user_inside', array('user_id' => $user_id, 'Date(createddate)' => $date), 'user_inside_id,user_inside_status');
            if ($type == "1")
            {
//            echo "<pre>";
//            print_r($user_inside);
//            die;
                if (empty($user_inside))
                {
                    if ($this->check_user_availibility($user_id))
                    {
                        $user_data = array(
                            'user_id' => $user_id,
                            'user_inside_status' => 'Inside',
                            'createddate' => date('Y-m-d H:i:s'),
                        );
                        $result = $this->common->insert_data($user_data, 'user_inside');
                        if ($result)
                        {
                            $this->data['status'] = TRUE;
                            $this->data['msg'] = 'Success';
                        }
                        else
                        {
                            $this->data['status'] = FALSE;
                            $this->data['msg'] = 'Something went wrong. Please try again';
                        }
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'User not found';
                    }
                }
                else
                {
                    if ($user_inside[0]['user_inside_status'] == "Outside")
                    {
                        $res = $this->common->update_data(array('user_inside_status' => 'Inside'), 'user_inside', 'user_id', $user_id);
                        if ($res)
                        {
                            $this->data['status'] = TRUE;
                            $this->data['msg'] = 'Success';
                        }
                        else
                        {
                            $this->data['status'] = FALSE;
                            $this->data['msg'] = 'Something went wrong. Please try again';
                        }
                    }
                    else
                    {
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'Success';
                    }
                }
            }
            else
            {
                if ($user_inside[0]['user_inside_status'] == "Inside")
                {
                    $res = $this->common->update_data(array('user_inside_status' => 'Outside'), 'user_inside', 'user_id', $user_id);
                    if ($res)
                    {
                        $this->data['status'] = TRUE;
                        $this->data['msg'] = 'Success';
                    }
                    else
                    {
                        $this->data['status'] = FALSE;
                        $this->data['msg'] = 'Something went wrong. Please try again';
                    }
                }
                else
                {
                    $this->data['status'] = TRUE;
                    $this->data['msg'] = 'Success';
                }
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'userid required';
        }
        echo json_encode($this->data);
        die();
    }

    public function order_detail()
    {
        $order_id = $this->input->post('order_id');
        if ($order_id != '')
        {
            $product_array = array();
            $order_detail = $this->webservice_model->get_order_detail($order_id);
            $order_product_detail = $this->webservice_model->get_product_detail($order_id);
//            $order_product_detail = $this->common->select_database_id('jeweler_order_product', 'order_id', $order_id, "*");
//            echo "<pre>";
//            print_r($order_product_detail);
//            die;
            if (!empty($order_product_detail))
            {
                for ($i = 0; $i < count($order_product_detail); $i++)
                {

                    $pro_id = $order_product_detail[$i]['pro_id'];
                    $pro_images = $this->get_product_image($pro_id);

                    $product_array['product_detail'][$i]['pro_id'] = $pro_id;
                    $product_array['product_detail'][$i]['pro_name'] = $order_product_detail[$i]['pro_name'];
                    $product_array['product_detail'][$i]['pro_quantity'] = $order_product_detail[$i]['order_pro_quantity'];
                    $product_array['product_detail'][$i]['pro_size'] = $order_product_detail[$i]['order_pro_size'];
                    $product_array['product_detail'][$i]['pro_price'] = $order_product_detail[$i]['order_pro_price'];
                    if (!empty($pro_images))
                    {
                        $product_array['product_detail'][$i]['pro_image'] = $pro_images[0]['pro_image'];
                    }
                    else
                    {
                        $product_array['product_detail'][$i]['pro_image'] = base_url() . $this->config->item('noimage');
                    }
                    $this->data['status'] = TRUE;
                    $this->data['msg'] = 'Success';
                    $this->data['data'] = $product_array;
                }
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'Something went wrong. Please try again';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'order id required';
        }
        echo json_encode($this->data);
        die();
    }

    public function payment_credentails()
    {
        $uuid = $this->input->post('jeweler_id');
        $type = $this->input->post('type');
        $jeweler_id = $this->get_jeweler_id_from_uuid($uuid);
        if ($jeweler_id != '' && $type != '')
        {
            $payment_data = $this->webservice_model->get_payment_detail($jeweler_id, $type);
            if (!empty($payment_data))
            {
                $this->data['status'] = TRUE;
                $this->data['msg'] = 'Success';
                $this->data['data'] = $payment_data;
            }
            else
            {
                $this->data['status'] = FALSE;
                $this->data['msg'] = 'Something went wrong. Please try again.';
            }
        }
        else
        {
            $this->data['status'] = FALSE;
            $this->data['msg'] = 'all field required';
        }
        echo json_encode($this->data);
        die();
    }

    public function test()
    {
//        echo (phpinfo());die;
        $message = 'My first push notification!';
        $this->send_ios_push_notification('952c766a6b9bc1794c5e69a665b926352cff65c68edef709f7a3a26d571c6e9b', $message, $screen_name = '111');
    }

    function send_ios_push_notification($notification_id, $noify_message, $screen_name)
    {

//        ////////////////////////////////////////////////////////////////////////////////
        $path = __DIR__ . '/JewellerDevelopment.pem';
//if(file_exists($path)){echo "yes";}else{echo "no";}die;
        // Put your device token here (without spaces):
        $deviceToken = '952c766a6b9bc1794c5e69a665b926352cff65c68edef709f7a3a26d571c6e9b';
// Put your private key's passphrase here:
        $passphrase = 'nd1234567879';
// Put your alert message here:
        $message = 'Hello, This is testing of pushnotification';
////////////////////////////////////////////////////////////////////////////////
        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $path);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
// Open a connection to the APNS server
        $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);
        echo 'Connected to APNS' . PHP_EOL;
// Create the payload body
        $body['aps'] = array(
            'alert' => array(
                'body' => $message,
                'action-loc-key' => 'Bango App',
            ),
            'badge' => 2,
            'sound' => 'oven.caf',
        );
// Encode the payload as JSON
        $payload = json_encode($body);
// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));
        if (!$result)
            echo 'Message not delivered' . PHP_EOL;
        else
            echo 'Message successfully delivered' . PHP_EOL;
// Close the connection to the server
        fclose($fp);
    }

    public function test_notification()
    {
        $date = date('m-d-Y');
        $image_path = '';
        $gcmreg_id = "APA91bFTUqH5mP-dHUGb1XHSLh1K5NfW2XgGmOG_G-I75wcr-99xPO4PUaoIDtNs-nR87EmXa6cdct5b2KWFG3GJ-v6h8kZ9d8sgARJtj6msWOQJEufyMR6NTtWFJ4O1uyNRYmAKcnji";
        $registatoin_ids = array($gcmreg_id);
        $notificationmessage = "hi parth...";
        $notification_message = array("message" => $notificationmessage, 'date_time' => $date, 'user_image' => $image_path);
        $res_gcm = $this->send_notification($registatoin_ids, $notification_message);
        if ($res_gcm)
        {
            echo 'Message :' . $notificationmessage . 'sent to User_name successfully';
        }
        else
        {
            echo "fail" . '<br/>';
            echo $res_gcm;
        }
    }

}
